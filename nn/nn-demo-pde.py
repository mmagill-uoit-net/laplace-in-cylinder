import numpy as np
import matplotlib as mpl
mpl.use('Agg')
import matplotlib.pyplot as plt
import tensorflow as tf
import pandas as pd
import sklearn.model_selection as sms
import sys
import time
from mpl_toolkits.mplot3d import Axes3D

tf.reset_default_graph()

def PlotOnDomain(output):
    Nx = 100
    Ny = 100
    curx = np.linspace(-0.5*scale,0.5*scale,Nx)
    cury = np.linspace(-0.5*scale,0.5*scale,Ny)
    xx,yy = np.meshgrid(curx,cury)
    data = sess.run(output,{x:xx.flatten(),y:yy.flatten()})
    fig = plt.figure()
    # ax = fig.gca(projection='3d')
    # ax.plot_surface(xx,yy,data.reshape(Nx,Ny))
    # plt.pcolor(xx,yy,data.reshape(Nx,Ny))
    plt.contourf(xx,yy,data.reshape(Nx,Ny),20)
    plt.colorbar()
    plt.xlabel('X')
    plt.ylabel('Y')
    # plt.show()
    

#########################################################################################


seed = int(sys.argv[1])
LambdaR = float(sys.argv[2])


scale = 12.**0.5
xmin = -scale*0.5
ymin = -scale*0.5
xmax = +scale*0.5
ymax = +scale*0.5


printfreq = 1

#L_batch = 10000                 # Need large enough to exploit GPUs
L_batch = 100                 # Need large enough to exploit GPUs
#L_batch = 1000                 # Need large enough to exploit GPUs
num_batch = 10                    # Need large enough to minimize fraction of time spent saving
num_repeat = 100                  # Intended to improve gradient descent
stallmax = 10                    # Max number of epochs since improvement before termination

# Turn off residuals for now
L_res = 1000

# num_lay = 1
# num_hid = 10

# num_lay = 1
# num_hid = 120

# num_lay = 2
# num_hid = 20

num_lay = 3
num_hid = 14

# num_lay = 1
# num_hid = 30000

# num_lay = 4
# num_hid = 12

# num_lay = 5
# num_hid = 10

num_lay = 7
num_hid = 10

# num_lay = 10
# num_hid = 100

casename = ( ('%d-%d-%.e-'%(num_lay,num_hid,LambdaR)) +
             ('%d-%d-%d-%d-'%(L_batch,num_batch,num_repeat,stallmax)) +
             ('%d'%seed) )

np.random.seed(seed)
tf.set_random_seed(seed)

#activator = tf.nn.tanh
#activator = tf.nn.selu
def EffBackpropTanh(z):
    return tf.constant(1.7159)*tf.tanh(tf.constant(2./3.)*z)
activator = EffBackpropTanh
# def CustomTanh(z):
#     return tf.constant(1.7159)*tf.tanh(tf.constant(2./3.)*z) + tf.constant(0.10)*z
# activator = CustomTanh

optimizer = tf.train.AdamOptimizer()
#optimizer = tf.train.AdadeltaOptimizer(learning_rate=1.0)
#optimizer = tf.train.GradientDescentOptimizer(learning_rate=1e-2)

#########################################################################################


# Define the inputs
x = tf.placeholder(tf.float32, [None])
y = tf.placeholder(tf.float32, [None])

# Define function and boundary conditions
uref = tf.cos(tf.divide(np.pi*x,0.5*scale))*tf.exp(-tf.divide(np.pi*(y-ymin),0.5*scale))
g1 = tf.cos(tf.divide(np.pi*x,0.5*scale))
g2 = tf.exp(-tf.divide(np.pi*(ymax-ymin),0.5*scale))*g1

# Define the network architecture of N
N_hidden_weights = [tf.get_variable("N0",shape=[2, num_hid],initializer=tf.glorot_uniform_initializer())]
N_hidden_biases = [tf.Variable(tf.zeros([num_hid]))]
N_hidden_layers = [activator(tf.matmul(tf.transpose(tf.stack([x,y])),N_hidden_weights[0])+N_hidden_biases[0])]
for i in range(1,num_lay):
    if i%L_res==0:
        N_hidden_weights.append(tf.get_variable("N%d"%i,shape=[num_hid, num_hid],initializer=tf.glorot_uniform_initializer()))
        N_hidden_biases.append(tf.Variable(tf.zeros([num_hid])))
        N_hidden_layers.append(activator(tf.matmul(N_hidden_layers[i-1],N_hidden_weights[i])+N_hidden_biases[i])+
                               N_hidden_layers[i-L_res])
    else:
        N_hidden_weights.append(tf.get_variable("N%d"%i,shape=[num_hid, num_hid],initializer=tf.glorot_uniform_initializer()))
        N_hidden_biases.append(tf.Variable(tf.zeros([num_hid])))
        N_hidden_layers.append(activator(tf.matmul(N_hidden_layers[i-1],N_hidden_weights[i])+N_hidden_biases[i]))
N_output_weights = tf.get_variable("Nout",shape=[num_hid,1],initializer=tf.glorot_uniform_initializer())
N_output_biases = tf.Variable(tf.zeros([1]))
N_output = tf.matmul(N_hidden_layers[-1],N_output_weights)+N_output_biases
N_output = tf.squeeze(N_output)

# Define the network architecture of N on xmin
x_xmin = tf.placeholder(tf.float32, [None])
Nxmin_hidden_layers = [activator(tf.matmul(tf.transpose(tf.stack([x_xmin,y])),N_hidden_weights[0])+N_hidden_biases[0])]
for i in range(1,num_lay):
    if i%L_res==0:
        Nxmin_hidden_layers.append(activator(tf.matmul(Nxmin_hidden_layers[i-1],N_hidden_weights[i])+N_hidden_biases[i])+
                                   Nxmin_hidden_layers[i-L_res])
    else:
        Nxmin_hidden_layers.append(activator(tf.matmul(Nxmin_hidden_layers[i-1],N_hidden_weights[i])+N_hidden_biases[i]))
Nxmin_output = tf.matmul(Nxmin_hidden_layers[-1],N_output_weights)+N_output_biases
Nxmin_output = tf.squeeze(Nxmin_output)

# Define the network architecture of N on xmax
x_xmax = tf.placeholder(tf.float32, [None])
Nxmax_hidden_layers = [activator(tf.matmul(tf.transpose(tf.stack([x_xmax,y])),N_hidden_weights[0])+N_hidden_biases[0])]
for i in range(1,num_lay):
    if i%L_res==0:
        Nxmax_hidden_layers.append(activator(tf.matmul(Nxmax_hidden_layers[i-1],N_hidden_weights[i])+N_hidden_biases[i])+
                                   Nxmax_hidden_layers[i-L_res])
    else:
        Nxmax_hidden_layers.append(activator(tf.matmul(Nxmax_hidden_layers[i-1],N_hidden_weights[i])+N_hidden_biases[i]))
Nxmax_output = tf.matmul(Nxmax_hidden_layers[-1],N_output_weights)+N_output_biases
Nxmax_output = tf.squeeze(Nxmax_output)

# Define the network architecture of N on ymin
y_ymin = tf.placeholder(tf.float32, [None])
Nymin_hidden_layers = [activator(tf.matmul(tf.transpose(tf.stack([x,y_ymin])),N_hidden_weights[0])+N_hidden_biases[0])]
for i in range(1,num_lay):
    if i%L_res==0:
        Nymin_hidden_layers.append(activator(tf.matmul(Nymin_hidden_layers[i-1],N_hidden_weights[i])+N_hidden_biases[i])+
                                   Nymin_hidden_layers[i-L_res])
    else:
        Nymin_hidden_layers.append(activator(tf.matmul(Nymin_hidden_layers[i-1],N_hidden_weights[i])+N_hidden_biases[i]))
Nymin_output = tf.matmul(Nymin_hidden_layers[-1],N_output_weights)+N_output_biases
Nymin_output = tf.squeeze(Nymin_output)

# Define the network architecture of N on ymax
y_ymax = tf.placeholder(tf.float32, [None])
Nymax_hidden_layers = [activator(tf.matmul(tf.transpose(tf.stack([x,y_ymax])),N_hidden_weights[0])+N_hidden_biases[0])]
for i in range(1,num_lay):
    if i%L_res==0:
        Nymax_hidden_layers.append(activator(tf.matmul(Nymax_hidden_layers[i-1],N_hidden_weights[i])+N_hidden_biases[i])+
                                   Nymax_hidden_layers[i-L_res])
    else:
        Nymax_hidden_layers.append(activator(tf.matmul(Nymax_hidden_layers[i-1],N_hidden_weights[i])+N_hidden_biases[i]))
Nymax_output = tf.matmul(Nymax_hidden_layers[-1],N_output_weights)+N_output_biases
Nymax_output = tf.squeeze(Nymax_output)



#########################################################################################

# Define training regimen for N
u = N_output
ux_xmin = tf.gradients(Nxmin_output,x_xmin)[0]
ux_xmax = tf.gradients(Nxmax_output,x_xmax)[0]
uxx = tf.gradients(tf.gradients(u,x),x)[0]
uyy = tf.gradients(tf.gradients(u,y),y)[0]
N_loss = tf.reduce_mean(tf.square(uxx+uyy) +
                        tf.square(Nymin_output-g1) +
                        tf.square(Nymax_output-g2) +
                        tf.square(ux_xmin) +
                        tf.square(ux_xmax) )
N_reg = tf.contrib.layers.apply_regularization(
    tf.contrib.layers.l1_regularizer(LambdaR),
    weights_list=N_hidden_weights)
N_totloss = N_loss              # Start without regularization
train_N = optimizer.minimize(N_totloss)


#########################################################################################

# config = tf.ConfigProto(
#         device_count = {'GPU': 0}
#     )
# sess = tf.Session(config=config)
init = tf.global_variables_initializer()
sess = tf.Session()
sess.run(init)
saver = tf.train.Saver()


# Train N
I = np.ones(L_batch)
def RunTraining_N():
    global bestloss,curtime,timesince,losses,norms
    epochclock_start = time.time()    
    # Train on random inputs
    for i in range(num_batch):    # Run over different datasets
        curx = xmin + np.random.rand(L_batch) * (xmax-xmin)
        cury = ymin + np.random.rand(L_batch) * (ymax-ymin)
        for j in range(num_repeat): # Run over the same dataset repeatedly
            sess.run(train_N, {x:curx,y:cury,
                               x_xmin:xmin*I,x_xmax:xmax*I,
                               y_ymin:ymin*I,y_ymax:ymax*I})
    # Get current loss and norm
    testx = xmin + np.random.rand(L_batch) * (xmax-xmin)
    testy = ymin + np.random.rand(L_batch) * (ymax-ymin)
    curloss = sess.run(N_loss, {x:testx,y:testy,
                                x_xmin:xmin*I,x_xmax:xmax*I,
                                y_ymin:ymin*I,y_ymax:ymax*I})
    losses.append(curloss)
    curnorm = sess.run(N_reg)
    norms.append(curnorm)
    totloss = sess.run(N_totloss, {x:testx,y:testy,
                                   x_xmin:xmin*I,x_xmax:xmax*I,
                                   y_ymin:ymin*I,y_ymax:ymax*I})
    # Track best loss so far
    if totloss < bestloss:
        bestloss = totloss
        timesince = 0
        saveclock_start = time.time()
        saver.save(sess, "models/%s.ckpt"%casename)
        save_elapsed = time.time() - saveclock_start
        print "Saving took %.3e seconds."%save_elapsed
    if curtime%printfreq==0:
        print (("Training N; "+
                "Epoch % 4d; "+
                "Loss = %.2e; "+
                "Reg = %.2e; "+
                "B.S.F. = %.2e; "+
                "Epochs since B.S.F. = % 4d / % 4d")%
               (curtime,
                curloss,curnorm,bestloss,
                timesince,stallmax))
    curtime=curtime+1
    timesince=timesince+1
    epoch_elapsed = time.time() - epochclock_start
    print "Epoch took %.3e seconds."%epoch_elapsed    
    return timesince>stallmax

# Run
runclock_start = time.time()

global bestloss,curtime,timesince,losses,norms
bestloss=10000.0
curtime=0
timesince=0
losses = []
norms = []

# Pretraining without regularization didn't work very well
# Seems the discontinuity in the loss function just breaks things
# Might work with a very small LambdaR, or ramping it up slowly
# But now I am going to try no pretraining
# while 1==1:
#     endflag = RunTraining_N()
#     if endflag:
#         break
# run_elapsed = time.time() - runclock_start

# print "Applying regularization."
N_totloss = N_loss+N_reg
train_N = optimizer.minimize(N_totloss)
bestloss=10000.0
timesince=0

while 1==1:
    endflag = RunTraining_N()
    if endflag:
        break
run_elapsed = time.time() - runclock_start

print "Total runtime was %.3e seconds in %d epochs."%(run_elapsed,curtime)
#losses.append(run_elapsed)      # Very dirty workaround for now.
losses = np.array(losses)
norms = np.array(norms)
np.save('losses/loss_%s.npy'%casename,np.vstack([losses,norms]))

saver.restore(sess, "models/%s.ckpt"%casename)


plt.semilogy(losses,label='loss')
plt.semilogy(norms,label='L1')
plt.semilogy(losses+norms,'k--',label='Total')
plt.legend()
plt.title('Runtime: %d seconds'%run_elapsed)
plt.xlabel('Epoch')
plt.ylabel('Loss')
plt.savefig('plots/direct/%s_loss.png'%casename,dpi=200)
plt.close()
PlotOnDomain(u)
plt.title('u')
plt.savefig('plots/direct/%s_u.png'%casename,dpi=200)
plt.close()
PlotOnDomain(uref)
plt.title('uref')
plt.savefig('plots/direct/%s_uref.png'%casename,dpi=200)
plt.close()
PlotOnDomain(u-uref)
plt.title('u-uref')
plt.savefig('plots/direct/%s_err.png'%casename,dpi=200)
plt.close()


# Plot neuron activations
Nx = 100
Ny = 100
curx = np.linspace(-0.5*scale,0.5*scale,Nx)
cury = np.linspace(-0.5*scale,0.5*scale,Ny)
xx,yy = np.meshgrid(curx,cury)
for i in range(num_lay):
    for j in range(num_hid):
        data = sess.run(N_hidden_layers[i],{x:xx.flatten(),y:yy.flatten()})[:,j]
        plt.contourf(xx,yy,data.reshape(Nx,Ny),
                     20,vmin=-1.7159,vmax=1.7159)
        plt.colorbar()
        plt.savefig('plots/direct/%s_n-%d-%d.png'%(casename,i,j),dpi=200)
        plt.close()
