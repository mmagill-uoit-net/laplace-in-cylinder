import numpy as np
import matplotlib as mpl
#mpl.use('Agg')
import matplotlib.pyplot as plt
import tensorflow as tf
import pandas as pd
import sklearn.model_selection as sms
import sys
import time
from mpl_toolkits.mplot3d import Axes3D

tf.reset_default_graph()

def PlotOnDomain(output):
    Nx = 100
    Ny = 100
    curx = np.linspace(xmin,xmax,Nx)
    cury = np.linspace(ymin,ymax,Ny)
    xx,yy = np.meshgrid(curx,cury)
    data = sess.run(output,{x:xx.flatten(),y:yy.flatten()})
    fig = plt.figure()
    # ax = fig.gca(projection='3d')
    # ax.plot_surface(xx,yy,data.reshape(Nx,Ny))
    # plt.pcolor(xx,yy,data.reshape(Nx,Ny))
    plt.contourf(xx,yy,data.reshape(Nx,Ny),20)
    plt.colorbar()
    plt.xlabel('X')
    plt.ylabel('Y')
    # plt.show()
    return xx,yy
    

#########################################################################################


seed = int(sys.argv[1])
LambdaR = float(sys.argv[2])


# Rsp = 20.0
# Rgap = 2000.0
# Hgap = 800.0
# Lscale = (Rgap*Hgap)**0.5
# Rsp  /= Lscale
# Rgap /= Lscale
# Hgap /= Lscale
# xmin = 0.0  
# xmax = Rgap 
# ymin = -0.5*Hgap
# ymax = +0.5*Hgap

scale = 12**0.5
Rsp = 0.01   * scale 
xmin = -0.5 * scale
xmax = +0.5 * scale
ymin = -0.5 * scale
ymax = +0.5 * scale


printfreq = 1

#L_batch = 10000                 # Need large enough to exploit GPUs
L_batch = 100                 # Need large enough to exploit GPUs
# L_batch = 1000                 # Need large enough to exploit GPUs
num_batch = 10                    # Need large enough to minimize fraction of time spent saving
#num_batch = 1                    # Need large enough to minimize fraction of time spent saving
num_repeat = 100                  # Intended to improve gradient descent
#num_repeat = 1                  # Intended to improve gradient descent
stallmax = 5                    # Max number of epochs since improvement before termination
L_batch_test = 10000                 # Need large enough to exploit GPUs


num_lay = 3
num_hid = 12

casename = ( ('%d-%d-%.e-'%(num_lay,num_hid,LambdaR)) +
             ('%d-%d-%d-%d-'%(L_batch,num_batch,num_repeat,stallmax)) +
             ('%d'%seed) )

np.random.seed(seed)
tf.set_random_seed(seed)

#activator = tf.nn.tanh
def EffBackpropTanh(z):
    return tf.constant(1.7159)*tf.tanh(tf.constant(2./3.)*z)
activator = EffBackpropTanh

optimizer = tf.train.AdamOptimizer()
#optimizer = tf.train.GradientDescentOptimizer(1e-3)

#########################################################################################


# Define the inputs
x = tf.placeholder(tf.float32, [None])
y = tf.placeholder(tf.float32, [None])

# Define the network architecture of N
N_hidden_weights = [tf.get_variable("N0",shape=[2, num_hid],initializer=tf.glorot_uniform_initializer())]
N_hidden_biases = [tf.Variable(tf.zeros([num_hid]))]
N_hidden_layers = [activator(tf.matmul(tf.transpose(tf.stack([x,y])),N_hidden_weights[0])+N_hidden_biases[0])]
for i in range(1,num_lay):
    N_hidden_weights.append(tf.get_variable("N%d"%i,shape=[num_hid, num_hid],initializer=tf.glorot_uniform_initializer()))
    N_hidden_biases.append(tf.Variable(tf.zeros([num_hid])))
    N_hidden_layers.append(activator(tf.matmul(N_hidden_layers[i-1],N_hidden_weights[i])+N_hidden_biases[i]))
N_output_weights = tf.get_variable("Nout",shape=[num_hid,1],initializer=tf.glorot_uniform_initializer())
N_output_biases = tf.Variable(tf.zeros([1]))
N_output = tf.matmul(N_hidden_layers[-1],N_output_weights)+N_output_biases
N_output = tf.squeeze(N_output)

# Define the network architecture of N on xmin
x_xmin = tf.placeholder(tf.float32, [None])
y_xmin = tf.placeholder(tf.float32, [None])
Nxmin_hidden_layers = [activator(tf.matmul(tf.transpose(tf.stack([x_xmin,y_xmin])),N_hidden_weights[0])+N_hidden_biases[0])]
for i in range(1,num_lay):
    Nxmin_hidden_layers.append(activator(tf.matmul(Nxmin_hidden_layers[i-1],N_hidden_weights[i])+N_hidden_biases[i]))
Nxmin_output = tf.matmul(Nxmin_hidden_layers[-1],N_output_weights)+N_output_biases
Nxmin_output = tf.squeeze(Nxmin_output)

# Define the network architecture of N on xmax
x_xmax = tf.placeholder(tf.float32, [None])
y_xmax = tf.placeholder(tf.float32, [None])
Nxmax_hidden_layers = [activator(tf.matmul(tf.transpose(tf.stack([x_xmax,y_xmax])),N_hidden_weights[0])+N_hidden_biases[0])]
for i in range(1,num_lay):
    Nxmax_hidden_layers.append(activator(tf.matmul(Nxmax_hidden_layers[i-1],N_hidden_weights[i])+N_hidden_biases[i]))
Nxmax_output = tf.matmul(Nxmax_hidden_layers[-1],N_output_weights)+N_output_biases
Nxmax_output = tf.squeeze(Nxmax_output)

# Define the network architecture of N on ymin
x_ymin = tf.placeholder(tf.float32, [None])
y_ymin = tf.placeholder(tf.float32, [None])
Nymin_hidden_layers = [activator(tf.matmul(tf.transpose(tf.stack([x_ymin,y_ymin])),N_hidden_weights[0])+N_hidden_biases[0])]
for i in range(1,num_lay):
    Nymin_hidden_layers.append(activator(tf.matmul(Nymin_hidden_layers[i-1],N_hidden_weights[i])+N_hidden_biases[i]))
Nymin_output = tf.matmul(Nymin_hidden_layers[-1],N_output_weights)+N_output_biases
Nymin_output = tf.squeeze(Nymin_output)

# Define the network architecture of N on ymax
x_ymax = tf.placeholder(tf.float32, [None])
y_ymax = tf.placeholder(tf.float32, [None])
Nymax_hidden_layers = [activator(tf.matmul(tf.transpose(tf.stack([x_ymax,y_ymax])),N_hidden_weights[0])+N_hidden_biases[0])]
for i in range(1,num_lay):
    Nymax_hidden_layers.append(activator(tf.matmul(Nymax_hidden_layers[i-1],N_hidden_weights[i])+N_hidden_biases[i]))
Nymax_output = tf.matmul(Nymax_hidden_layers[-1],N_output_weights)+N_output_biases
Nymax_output = tf.squeeze(Nymax_output)



#########################################################################################

# Define training regimen for N

# u and its derivatives
u = N_output
ux_xmin = tf.gradients(Nxmin_output,x_xmin)[0]
ux_xmax = tf.gradients(Nxmax_output,x_xmax)[0]
uy_ymin = tf.gradients(Nymin_output,y_ymin)[0]
ux = tf.gradients(u,x)[0]
uy = tf.gradients(u,y)[0]
uxx = tf.gradients(tf.gradients(u,x),x)[0]
uyy = tf.gradients(tf.gradients(u,y),y)[0]
#Lap = uxx+(1./(x-xmin))*ux+uyy
Lap_t_r = (x-xmin)*uxx+ux+(x-xmin)*uyy
#Lap_t_r = (x-xmin)*uxx+ux+(x-xmin)*uyy /(scale**2) # Multiplied by r for Jacobian, divided by domain measure

# Boundary condition
# BC1 = (tf.where(tf.less((x-xmin),Rsp),
#                 (Nymin_output+1.0)/Rsp,  # Divided by domain measure
#                 (uy_ymin)/(Rsp)) * # Divided by domain measure
#        (x-xmin))                # Multiplied by r for Jacobian
BC1 = (tf.where(tf.less((x-xmin),Rsp),
                (Nymin_output+1.0)/Rsp,  # Divided by domain measure
                (uy_ymin)/(Rsp)) * # Divided by domain measure
       (x-xmin))                # Multiplied by r for Jacobian
BC2 = (Nymax_output-1.0) * (x-xmin) / scale # Multiplied by r for Jacobian, divided by domain measure
BC3 = (ux_xmin) / scale
BC4 = (ux_xmax) / scale

# Loss
# N_loss = tf.reduce_mean(tf.abs(Lap_t_r) +
#                         tf.abs(BC1) +
#                         tf.abs(BC2) +
#                         tf.abs(BC3) +
#                         tf.abs(BC4) )
# N_loss = tf.reduce_mean(0.1*tf.square(Lap) +
#                         10.0*BC1 +
#                         BC2 +
#                         BC3 +
#                         BC4 )
# N_loss = tf.reduce_mean(0.01*tf.square(Lap_t_r) +
#                         BC1 +
#                         BC2 +
#                         BC3 +
#                         BC4 )
N_reg = tf.contrib.layers.apply_regularization(
    tf.contrib.layers.l1_regularizer(LambdaR),
    weights_list=N_hidden_weights)
N_totloss = N_loss+N_reg
train_N = optimizer.minimize(N_totloss)


#########################################################################################

# config = tf.ConfigProto(
#         device_count = {'GPU': 0}
#     )
# sess = tf.Session(config=config)
sess = tf.Session()
init = tf.global_variables_initializer()
sess.run(init)
saver = tf.train.Saver()


# Train N
def RunTraining_N():
    global bestloss,curtime,timesince,losses,norms
    epochclock_start = time.time()    
    # Train on random inputs
    for i in range(num_batch):    # Run over different datasets
        I = np.ones(L_batch)
        curx = xmin + np.random.rand(L_batch) * (xmax-xmin)
        cury = ymin + np.random.rand(L_batch) * (ymax-ymin)
        curx_xmin = xmin * I
        curx_xmax = xmax * I
        cury_ymin = ymin * I
        cury_ymax = ymax * I
        cury_xmin = ymin + np.random.rand(L_batch) * (ymax-ymin)
        curx_ymin = xmin + np.random.rand(L_batch) * (xmax-xmin)
        cury_xmax = ymin + np.random.rand(L_batch) * (ymax-ymin)
        curx_ymax = xmin + np.random.rand(L_batch) * (xmax-xmin)
        for j in range(num_repeat): # Run over the same dataset repeatedly
            sess.run(train_N, {x:curx,y:cury,
                               x_xmin:curx_xmin,x_xmax:curx_xmax,
                               y_ymin:cury_ymin,y_ymax:cury_ymax,
                               y_xmin:cury_xmin,y_xmax:cury_xmax,
                               x_ymin:curx_ymin,x_ymax:curx_ymax})
    # Get current loss and norm
    I = np.ones(L_batch_test)
    testx = xmin + np.random.rand(L_batch_test) * (xmax-xmin)
    testy = ymin + np.random.rand(L_batch_test) * (ymax-ymin)
    testx_xmin = xmin * I
    testx_xmax = xmax * I
    testy_ymin = ymin * I
    testy_ymax = ymax * I
    testy_xmin = ymin + np.random.rand(L_batch_test) * (ymax-ymin)
    testx_ymin = xmin + np.random.rand(L_batch_test) * (xmax-xmin)
    testy_xmax = ymin + np.random.rand(L_batch_test) * (ymax-ymin)
    testx_ymax = xmin + np.random.rand(L_batch_test) * (xmax-xmin)
    curloss = sess.run(N_loss, {x:testx,y:testy,
                                x_xmin:testx_xmin,x_xmax:testx_xmax,
                                y_ymin:testy_ymin,y_ymax:testy_ymax,
                                y_xmin:testy_xmin,y_xmax:testy_xmax,
                                x_ymin:testx_ymin,x_ymax:testx_ymax})
    losses.append(curloss)
    curnorm = sess.run(N_reg)
    norms.append(curnorm)
    totloss = sess.run(N_totloss, {x:testx,y:testy,
                                   x_xmin:testx_xmin,x_xmax:testx_xmax,
                                   y_ymin:testy_ymin,y_ymax:testy_ymax,
                                   y_xmin:testy_xmin,y_xmax:testy_xmax,
                                   x_ymin:testx_ymin,x_ymax:testx_ymax})
    # Track best loss so far
    if totloss < bestloss:
        bestloss = totloss
        timesince = 0
        saveclock_start = time.time()
        saver.save(sess, "models/%s.ckpt"%casename)
        save_elapsed = time.time() - saveclock_start
        print "Saving took %.3e seconds."%save_elapsed
    if curtime%printfreq==0:
        print (("Training N; "+
                "Epoch % 4d; "+
                "Loss = %.2e; "+
                "Reg = %.2e; "+
                "B.S.F. = %.2e; "+
                "Epochs since B.S.F. = % 4d / % 4d")%
               (curtime,
                curloss,curnorm,bestloss,
                timesince,stallmax))
    curtime=curtime+1
    timesince=timesince+1
    epoch_elapsed = time.time() - epochclock_start
    print "Epoch took %.3e seconds."%epoch_elapsed    
    return timesince>stallmax

# Run
runclock_start = time.time()
bestloss=10000.0
curtime=0
timesince=0
losses = []
norms = []
while 1==1:
    endflag = RunTraining_N()
    if endflag:
        break
run_elapsed = time.time() - runclock_start
print "Total runtime was %.3e seconds in %d epochs."%(run_elapsed,curtime)
losses = np.array(losses)
norms = np.array(norms)
np.save('losses/loss_%s.npy'%casename,np.vstack([losses,norms]))

saver.restore(sess, "models/%s.ckpt"%casename)


plt.semilogy(losses,label='loss')
plt.semilogy(norms,label='L1')
plt.semilogy(losses+norms,'k--',label='Total')
plt.legend()
plt.title('Runtime: %d seconds'%run_elapsed)
plt.xlabel('Epoch')
plt.ylabel('Loss')
plt.savefig('plots/direct/%s_loss.png'%casename,dpi=200)
plt.close()

Nx = 100
Ny = 100
xx,yy = PlotOnDomain(u)
plt.streamplot(xx,yy,
               sess.run(ux,{x:xx.flatten(),y:yy.flatten()}).reshape(Nx,Ny),
               sess.run(uy,{x:xx.flatten(),y:yy.flatten()}).reshape(Nx,Ny),
               color='k')
plt.title('u')
plt.savefig('plots/direct/%s_u.png'%casename,dpi=200)
plt.close()

PlotOnDomain(Lap_t_r/(x-xmin))
plt.title('Lap')
plt.savefig('plots/direct/%s_Lap.png'%casename,dpi=200)
plt.close()

# Plot boundary conditions
N = 100
curx = np.linspace(xmin,xmax,N)
cury = np.linspace(ymin,ymax,N)
I = np.ones(N)

plt.plot(curx,
         sess.run(u,
                  {x:curx,y:ymin*I}))
plt.axhline(-1.0)
plt.xlabel('r')
plt.ylabel('u(z=zmin)')
plt.tight_layout()
plt.savefig('plots/direct/%s_BC1a.png'%casename,dpi=200)
plt.close()

plt.plot(curx,
         sess.run(uy_ymin,
                  {x_ymin:curx,y_ymin:ymin*I}))
plt.axhline(0.0)
plt.xlabel('r')
plt.ylabel('u_z(z=zmin)')
plt.tight_layout()
plt.savefig('plots/direct/%s_BC1b.png'%casename,dpi=200)
plt.close()

plt.plot(curx,
         sess.run(u,
                  {x:curx,y:ymax*I}))
plt.axhline(+1.0)
plt.xlabel('r')
plt.ylabel('u(z=zmax)')
plt.tight_layout()
plt.savefig('plots/direct/%s_BC2.png'%casename,dpi=200)
plt.close()

plt.plot(cury,
         sess.run(ux_xmin,
                  {y_xmin:cury,x_xmin:xmin*I}))
plt.axhline(0.0)
plt.xlabel('z')
plt.ylabel('u_r(r=0)')
plt.tight_layout()
plt.savefig('plots/direct/%s_BC3.png'%casename,dpi=200)
plt.close()

plt.plot(cury,
         sess.run(ux_xmax,
                  {y_xmax:cury,x_xmax:xmax*I}))
plt.axhline(0.0)
plt.xlabel('z')
plt.ylabel('u_r(r=rmax)')
plt.tight_layout()
plt.savefig('plots/direct/%s_BC4.png'%casename,dpi=200)
plt.close()


# # Plot neuron activations
# Nx = 100
# Ny = 100
# curx = np.linspace(xmin,xmax,Nx)
# cury = np.linspace(ymin,ymax,Ny)
# xx,yy = np.meshgrid(curx,cury)
# for i in range(num_lay):
#     for j in range(num_hid):
#         data = sess.run(N_hidden_layers[i],{x:xx.flatten(),y:yy.flatten()})[:,j]
#         plt.contourf(xx,yy,data.reshape(Nx,Ny),
#                      20,vmin=-1.7159,vmax=1.7159)
#         plt.colorbar()
#         plt.savefig('plots/direct/%s_n-%d-%d.png'%(casename,i,j),dpi=200)
#         plt.close()
