import os


# Half nm resn seems more than good enough for 4x5 pore
resn = 1./2

#for rmax in [30,50]:
for rmax in [70]:

    zmax = rmax
    rfine = rmax/10
    zfine = zmax/10

    Nr = int(round(rmax/resn))
    Nz = Nr

    Nrfine = Nr/10
    Nzfine = Nz/10
    Nrcoarse = Nr - Nrfine
    Nzcoarse = Nz - Nzfine

    print (rmax,zmax,rfine,zfine,Nrfine,Nzfine,Nrcoarse,Nzcoarse)

    os.system('time ./geometry.exe %f %f %f %f %d %d %d %d' %
              (rmax,zmax,rfine,zfine,Nrfine,Nzfine,Nrcoarse,Nzcoarse))
