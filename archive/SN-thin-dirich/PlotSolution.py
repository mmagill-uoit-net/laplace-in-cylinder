import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


fig, axarr = plt.subplots(2,1)
for rmax in [30,50,70]:

    # Select case
    zmax = rmax
    rfine = rmax/10
    zfine = rfine
    Nrfine = rfine*2
    Nzfine = Nrfine
    Nrcoarse = Nrfine*9
    Nzcoarse = Nrcoarse

    Nr = Nrfine + Nrcoarse + 1
    Nz = Nzfine + Nzcoarse + 1

    # Load data
    u = np.loadtxt('data/soln_%03d_%03d_%03d_%03d_%03d_%03d_%03d_%03d.dat'%
                   (Nrfine,Nrcoarse,rmax,rfine,Nzfine,Nzcoarse,zmax,zfine,))
    U = np.resize(u,(Nz,Nr))

    # Coordinates
    rinner = np.linspace(0,rfine,Nrfine+1)
    router = np.linspace(rfine,rmax,Nrcoarse+1)
    r = np.concatenate([rinner,router[1:]])
    zinner = np.linspace(0,zfine,Nzfine+1)
    zouter = np.linspace(zfine,zmax,Nzcoarse+1)
    z = np.concatenate([zinner,zouter[1:]])
    dr = np.diff(r)
    dz = np.diff(z)
    R = np.outer(np.ones(Nz),r)
    Z = np.outer(z,np.ones(Nr))
    DR = np.outer(np.ones(Nz),dr)
    DZ = np.outer(dz,np.ones(Nr))
    
    # Calculate gradient (cylindrical coordinates with symmetry means this is okay)
    # Non-uniform mesh means this is not okay
    # dr = r[1] - r[0]
    # dz = z[1] - z[0]
    # Ez,Er = np.gradient(-U,dz,dr)
    Er = np.diff(-U,axis=1) / DR
    Ez = np.diff(-U,axis=0) / DZ
    Er = np.concatenate((np.zeros((Nz,1)),Er),axis=1)
    Ez = np.concatenate((Ez,np.zeros((1,Nr))),axis=0)
    
    # Double-side everything
    UU = np.concatenate((np.fliplr(U),U),axis=1)
    RR = np.concatenate((-np.fliplr(R),R),axis=1)
    ZZ = np.concatenate((np.fliplr(Z),Z),axis=1)
    EEr = np.concatenate((-np.fliplr(Er),Er),axis=1)
    EEz = np.concatenate((np.fliplr(Ez),Ez),axis=1)
    
    # Plot
    fig = plt.figure()
    ax = plt.axes()
    # ax.pcolor(R,Z,U,vmin=-1.,vmax=1.)
    
    #ax = plt.axes(projection='3d')
    #ax.plot_surface(RR, ZZ, UU, cmap=plt.cm.jet, rstride=1, cstride=1, linewidth=0)
    
    #ax.pcolor(RR,ZZ,UU,vmin=-1.,vmax=1.)
    #ax.grid()
    #V = np.linspace(0.9,1.0,30)
    #V = np.concatenate((-V[::-1],V),axis=1)
    #ax.contour(RR,ZZ,UU,V,colors='k',lw=6)
    #ax.contour(R,Z,U,V,colors='k',lw=6)
    
    # Streamplot doesn't work on uneven grids
    ax.streamplot(RR,ZZ,EEr,EEz)

    # Line plot alone axis
#    axarr[0].plot(z,U[:,0],label='rmax = %d'%rmax)
#    axarr[1].semilogy(z,U[:,0]+1)
    axarr[0].plot(z[1:],np.diff(U[:,0]),label='rmax = %d'%rmax)
    axarr[1].semilogy(z[1:],-np.diff(U[:,0]))

# Theoretical solution
a = 6. # Pore radius
c = a  # nu0 ~ 0
# z = c sinh mu sin nu
Utheory = np.arctan(z/c) * ( -2. / np.pi )
#Utheory = Utheory - Utheory[-1]
Utheory = Utheory * 0.9
#Utheory = Utheory - 1
#axarr[0].plot(z,Utheory,'k--',label='theory')
#axarr[1].semilogy(z,Utheory+1,'k--')
axarr[0].plot(z[1:],np.diff(Utheory),'k--',label='theory')
axarr[1].semilogy(z[1:],-np.diff(Utheory),'k--')

# Plot
axarr[0].legend()
plt.show()


