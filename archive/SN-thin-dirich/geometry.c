#include "laplace-in-cylinder.h"


int main(int argc, char* argv[]) {

  ////////////////////////////////////////////////////////////////
  //// Define domain, mesh, CLI
  if ( argc != 9 ) { printf("%s [rmax] [zmax] [rfine] [zfine] [Nrfine] [Nzfine] [Nrcoarse] [Nzcoarse] \n",argv[0]); return 1; }
  double rmax = strtof(argv[1],NULL);
  double zmax = strtof(argv[2],NULL);
  double rfine = strtof(argv[3],NULL);
  double zfine = strtof(argv[4],NULL);
  int Nrfine = strtol(argv[5],NULL,10); 
  int Nzfine = strtol(argv[6],NULL,10); 
  int Nrcoarse = strtol(argv[7],NULL,10); 
  int Nzcoarse = strtol(argv[8],NULL,10); 
  // So that resolutions are easy to see from input
  int Nr = Nrfine + Nrcoarse + 1;
  int Nz = Nzfine + Nzcoarse + 1;
  ////
  ////////////////////////////////////////////////////////////////

  //// DON'T CHANGE THIS PART
  //// Set up A,f,u and initialize w/o BCs
  // Initialize operator A, RHS vector F, and solution U
  double** A = malloc(sizeof(double*)*Nz*Nr); 
  double* f = malloc(sizeof(double)*Nz*Nr); 
  double* u = malloc(sizeof(double)*Nz*Nr);
  for (int i=0; i<Nz*Nr; i++) {
    A[i] = malloc(sizeof(double)*Nz*Nr);
    for (int j=0; j<Nz*Nr; j++) { A[i][j] = 0; }
    f[i] = 0; u[i] = 0;
  }
  // R mesh
  double* dr = malloc(sizeof(double)*(Nr-1));
  double smalldr = rfine / (double) Nrfine;
  for (int i=0; i<Nrfine; i++) { dr[i] = smalldr; }
  double bigdr = (rmax - rfine) / (double) Nrcoarse;
  for (int i=Nrfine; i<Nr-1; i++) { dr[i] = bigdr; }
  // Z mesh
  double* dz = malloc(sizeof(double)*(Nz-1));
  double smalldz = zfine / (double) Nzfine;
  for (int i=0; i<Nzfine; i++) { dz[i] = smalldz; }
  double bigdz = (zmax - zfine) / (double) Nzcoarse;
  for (int i=Nzfine; i<Nz-1; i++) { dz[i] = bigdz; }
  // Fill operator, ignoring boundaries
  FillOperatorWoBcs(A,Nz,Nr,dz,dr);
  ////

  ////////////////////////////////////////////////////////////////
  //// Define geometry, BCs
  //// Be careful about applying BCs in the same row twice
  //// e.g. Newmann then Dirichlet w.o reinitializing is wrong
  //// All conditions are homogeneous by default

  // Geometric parameters
  double rpore = 6.0; // nm
  double tpore = 0.5;  // Half-width of the pore

  // Find corresponding bins (radial mesh is adaptive)
  float rtmp = 0; int Nrpore = 0; int ind = 0;
  while ( rpore > rtmp ) { rtmp=rtmp+dr[ind]; Nrpore++; ind++; } Nrpore--;
  float ztmp = 0; int Ntpore = 0; ind = 0;
  while ( tpore > ztmp ) { ztmp=ztmp+dz[ind]; Ntpore++; ind++; }

  // Indices
  int k_r0; int k_rmax;
  int k_z0; int k_zmax;
  int k_tpore;
  int k_rpore; int k_inpore;

  // Apply homogenous Newmann BCs in r for (0<z<zmax,r=0)
  for (int i=1; i<Nz-1; i++) {
    k_r0 = i*Nr + 0;
    SetBC_NewmannR(A,Nz,Nr,k_r0,dr,+1);
  }

  // Apply non-homogeneous Dirichlet BCs for (z=zmax,all r)
  for (int j=0; j<Nr; j++) {
    k_zmax = (Nz-1)*Nr + j;
    SetBC_Dirichlet(A,Nz,Nr,k_zmax);
    f[k_zmax] = -1.;
  }

  // Apply non-homogenous Dirichlet BCs in r for (tpore<z<zmax,r=rmax)
  for (int i=Ntpore+1; i<Nz-1; i++) {
    k_rmax = i*Nr + (Nr-1);
    SetBC_Dirichlet(A,Nz,Nr,k_rmax);
    f[k_rmax] = -1.;
  }

  // Apply homogenous Newmann BCs in z for (z=tpore,rpore<r<=rmax)
  for (int j=Nrpore+1; j<Nr; j++) {
    k_tpore = Ntpore*Nr + j;
    SetBC_NewmannZ(A,Nz,Nr,k_tpore,dz,+1);
  }

  // Apply homogeneous Newmann BCs in r for (0<=z<=tpore,r=rpore)
  for (int i=0; i<Ntpore; i++) {
    k_rpore = i*Nr + Nrpore;
    SetBC_NewmannR(A,Nz,Nr,k_rpore,dr,-1);
  }

  // Apply homogeneous Dirichlet BCs for (zcis<z<ztrans,rpore<r<=rmax)
  for (int i=0; i<Ntpore; i++) {
    for (int j=Nrpore+1; j<Nr; j++) {
      k_inpore = i*Nr + j;
      SetBC_Dirichlet(A,Nz,Nr,k_inpore);
      f[k_inpore] = 1.;
    }
  }

  // Apply homogeneous Dirichlet BCs for (z=0,0<r<rpore)
  for (int j=0; j<Nrpore; j++) {
    k_inpore = 0*Nr + j;
    SetBC_Dirichlet(A,Nz,Nr,k_inpore);
    f[k_inpore] = 0.;
  }

  ////
  ////////////////////////////////////////////////////////////////

  //// DON'T CHANGE THIS PART
  //// Solve, output, exit
  // Solve using LAPACK
  SolveSystem(A,u,f,Nz,Nr);
  // Output
  char fout_name[256]; sprintf(fout_name,"data/soln_%03d_%03d_%03d_%03d_%03d_%03d_%03d_%03d.dat",
			       Nrfine,Nrcoarse,(int) round(rmax),(int) round(rfine),
			       Nzfine,Nzcoarse,(int) round(zmax),(int) round(zfine));
  FILE* fout =fopen(fout_name,"w");
  for (int i=0; i<Nz*Nr; i++) {
    fprintf(fout,"%.8e\n",u[i]);
  }
  // Release Memory
  fclose(fout); free(f); free(u); 
  for (int i=0; i<Nz*Nr; i++) {
    free(A[i]);
  } free(A);
  // Exit
  return 0;
  ////
}
