#include "laplace-in-cylinder.h"


int main(int argc, char* argv[]) {

  ////////////////////////////////////////////////////////////////
  /*
  //// Define domain, mesh, CLI
  if ( argc != 7 ) { printf("%s [rante] [zante] [rpore] [tpore] [Nr] [Nz] \n",argv[0]); return 1; }
  double rante = strtof(argv[1],NULL);     // Radius of antechamber
  double zante = strtof(argv[2],NULL);     // Width of antechamber
  double rpore = strtof(argv[3],NULL);    // nm
  double tpore = strtof(argv[4],NULL);    // Half-width of the pore
  double rmax = rante;
  double zmax = zante + tpore;
  int Nr = strtol(argv[5],NULL,10);       // Number of r nodes
  int Nz = strtol(argv[6],NULL,10);       // Number of z nodes
  ////
  */
  // USING EFFECTIVE DIMENSIONS -- SMALL PROBABILITY OF PARTICLES ENTERING A REGION WHERE FIELD IS UNDEFINED 
  float sigma = 5.;              // Sigma is 5 nm
  float rante = 500./sigma;      // The effective radius of the cavity:      r = 500 nm
  float rpore_filt = 17.5/sigma; // The effective radius of the filter pore: d = 35 +/- 10 nm
  float rpore_exit = 10.0/sigma; // The effective radius of the exit pore:   d = 6-20 nm
  float rmax = rante;
  float tante = 200./sigma;      // The effective thickness of the antechamber: t = 200 nm
  float tpore_filt = 50./sigma;  // The effective thickness of the filter:      t = 50 nm
  float tpore_exit = 20./sigma;  // The effective thickness of the exit pore:   t = 20 nm
  float zmax = tante+tpore_exit; // Go until the BOTTOM of the filter (NOT SIMULATING INSIDE FILTER)
  //  int N = 128;  //   13% of 32 GB
  //  int N = 256;  // >100% of 32 GB
  int N = 180;  //   50% of 32 GB
  int Nr = N;
  int Nz = N;
  ////////////////////////////////////////////////////////////////
  //// DON'T CHANGE THIS PART
  //// Set up A,f,u and initialize w/o BCs
  // Initialize operator A, RHS vector F, and solution U
  double** A = malloc(sizeof(double*)*Nz*Nr); 
  double* f = malloc(sizeof(double)*Nz*Nr); 
  double* u = malloc(sizeof(double)*Nz*Nr);
  for (int i=0; i<Nz*Nr; i++) {
    A[i] = malloc(sizeof(double)*Nz*Nr);
    for (int j=0; j<Nz*Nr; j++) { A[i][j] = 0; }
    f[i] = 0; u[i] = 0;
  }
  // R mesh
  double* dr = malloc(sizeof(double)*(Nr-1));
  for (int i=0; i<Nr-1; i++) { dr[i] = rmax/(Nr-1); }
  // Z mesh
  double* dz = malloc(sizeof(double)*(Nz-1));
  for (int i=0; i<Nz-1; i++) { dz[i] = zmax/(Nz-1); }
  // Fill operator, ignoring boundaries
  FillOperatorWoBcs(A,Nz,Nr,dz,dr);
  ////

  ////////////////////////////////////////////////////////////////
  //// Define geometry, BCs
  //// All conditions are homogeneous by default
  //// Be careful about applying BCs in the same row twice
  //// New BCs wil override previous BCs

  // Indices
  int k;

  // Apply Homogeneous Newmann BCs at r=0
  // ALWAYS NECESSARY BY MATH
  for (int i=0; i<Nz; i++) {
    k = i*Nr + 0;
    SetBC_NewmannR(A,Nz,Nr,k,dr,+1);
    f[k] = 0.;
  }

  // Apply Inhomogeneous Dirichlet BCs at zmax
  // BOTTOM OF NANOPOROUS FILTER MODELLED AS CONSTANT POTENTIAL
  for (int j=1; j<Nr-1; j++) {
    k = (Nz-1)*Nr + j;
    SetBC_Dirichlet(A,Nz,Nr,k);
    f[k] = -0.5;
  }

  // Apply Newmann BCs at r=rmax
  // INSULATED WALLS OF ANTECHAMBER
  for (int i=0; i<Nz; i++) {
    k = i*Nr + (Nr-1);
    SetBC_NewmannR(A,Nz,Nr,k,dr,-1);
    f[k] = 0.;
  }

  /* // Is this really necessary? Isn't it overridden below?
  // Apply Newmann BCs at bottom of cylinder
  // INSULATED WALL OF CYLINDER'S TOP
  for (int j=1; j<Nr-1; j++) {
    k = 0*Nr + j;
    SetBC_NewmannZ(A,Nz,Nr,k,dr,+1);
    f[k] = 0.;
  }
  */

  // Pore Geometry Parameters
  // Recall: dr = rpore/(Npore-1) and similar
  /* // Filter not currently modelled
  int jpore_filt = (int) round(rpore_filt/dr[0]) + 1;
  int ipore_filt = (int) round(tpore_filt/dz[0]) + 1;
  */
  int jpore_exit = (int) round(rpore_exit/dr[0]) + 1;
  int ipore_exit = (int) round(tpore_exit/dz[0]) + 1;

  // Override
  // Apply Dirichlet BCs at bottom of exit pore
  // BOTTOM OF EXIT PORE AT FIXED POTENTIAL
  for (int j=1; j<jpore_exit; j++) {
    k = 0*Nr + j;
    SetBC_Dirichlet(A,Nz,Nr,k);
    f[k] = 0.5;
  }
  
  // Internal BC
  // Apply Newmann BC at wall inside exit pore
  // EXIT PORE WALLS ARE INSULATING
  for (int i=0; i<ipore_exit; i++) {
    k = i*Nr + jpore_exit;
    SetBC_NewmannR(A,Nz,Nr,k,dr,-1);
    f[k] = 0.;
  }

  // Internal BC
  // Apply Newmann BC at wall of exit pore in antechamber
  // EXIT PORE WALLS ARE INSULATING
  for (int j=jpore_exit; j<Nr-1; j++) {
    k = ipore_exit*Nr + j;
    SetBC_NewmannZ(A,Nz,Nr,k,dz,+1);
    f[k] = 0.;
  }

  // Internal BC
  // Fill the interior of the cavity wall
  for (int i=0; i<ipore_exit; i++) {
    for (int j=jpore_exit+1; j<Nr-1; j++) {
      k = i*Nr + j;
      SetBC_Dirichlet(A,Nz,Nr,k);
      f[k] = 20.;
    }
  }


  ////
  ////////////////////////////////////////////////////////////////

  //// DON'T CHANGE THIS PART
  //// Solve, output, exit
  // Solve using LAPACK
  SolveSystem(A,u,f,Nz,Nr);
  // Output
  char fout_name[256]; sprintf(fout_name,"data/soln_%03d.dat",N);
  //  char fout_name[256]; sprintf(fout_name,"data/soln.dat");
  FILE* fout =fopen(fout_name,"w");
  for (int i=0; i<Nz*Nr; i++) {
    fprintf(fout,"%.8e\n",u[i]);
  }
  // Release Memory
  fclose(fout); free(f); free(u); 
  for (int i=0; i<Nz*Nr; i++) {
    free(A[i]);
  } free(A);
  // Exit
  return 0;
  ////
}
