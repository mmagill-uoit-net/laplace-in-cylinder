import sys

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


# Select case
N = int(sys.argv[1])
Nr = N
Nz = N

# Coordinates used in the field solver geometry
sigma = 5.
rmax = 500./sigma
zmax = (200.+20.)/sigma


# Load data
u = np.loadtxt('data/soln_%03d.dat'%(N))
U = np.resize(u,(Nz,Nr))

# Coordinates
r = np.linspace(0,rmax,Nr)
z = np.linspace(0,zmax,Nz)
dr = r[1] - r[0]
dz = z[1] - z[0]
R = np.outer(np.ones(Nz),r)
Z = np.outer(z,np.ones(Nr))
DR = np.outer(np.ones(Nz),dr)
DZ = np.outer(dz,np.ones(Nr))

# Calculate gradient (cylindrical coordinates with symmetry means this is okay)
Ez,Er = np.gradient(-U,dz,dr)


# Reshape and output as a table for Espresso loader
outsize = Nr*Nz
with open('HDH_soln_%03d.dat'%N,'w') as outfile:
    for iz in range(Nz):
        for ir in range(Nr):
            k = ir*Nr + iz
            outfile.write( "%d %d %e %e\n"%(ir,iz,Er[iz][ir],Ez[iz][ir]))
