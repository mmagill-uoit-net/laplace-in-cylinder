import sys

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

# Initialize figure
fig = plt.figure()
ax = plt.axes()
#ax = fig.add_subplot(111, projection='3d')

# Select case
N = int(sys.argv[1])
Nr = N
Nz = N

# Coordinates used in the field solver geometry
sigma = 5.
rmax = 500./sigma
zmax = (200.+20.)/sigma
rexit = 2.0 # *sigma
rcavi = 40. # *sigma

# Load data
u = np.loadtxt('data/soln_%03d.dat'%(N))
U = np.resize(u,(Nz,Nr))

# Coordinates
r = np.linspace(0,rmax,Nr)
z = np.linspace(0,zmax,Nz)
dr = r[1] - r[0]
dz = z[1] - z[0]
R = np.outer(np.ones(Nz),r)
Z = np.outer(z,np.ones(Nr))
DR = np.outer(np.ones(Nz),dr)
DZ = np.outer(dz,np.ones(Nr))

# Calculate gradient (cylindrical coordinates with symmetry means this is okay)
Ez,Er = np.gradient(-U,dz,dr)
E = np.sqrt(Ez**2 + Er**2)

# # Check the unmodified solution (no z symmetry in this system)
# ax.pcolor(R,Z,U,vmin=-1.,vmax=1.,cmap='flag')
# ax.grid()
# ax.streamplot(R,Z,Er,Ez)

# ## Plot E field as a surface
# #ax.plot_surface(R,Z,E)
# #ax.set_zlim(0,0.0005)

# # Plot
# plt.xlim([0,rmax])
# plt.ylim([0,zmax])
# plt.show()


# # Plot abs(E)(r) at z = zmax
# Er_top = Er[-1,:]
# Ez_top = Ez[-1,:]
# E_top = np.sqrt(Er_top**2 + Ez_top**2)
# plt.plot(r, E_top/E_top[0])
# plt.xlabel("Radius")
# plt.ylabel("Normalized Electric Field Magnitude at Bottom of NPF")
# plt.show()

# # Plot r*abs(E)(r) at z = zmax (crude estimate of capture prob)
# Er_top = Er[-1,:]
# Ez_top = Ez[-1,:]
# E_top = np.sqrt(Er_top**2 + Ez_top**2)
# metric = r*E_top
# metric = metric / np.max(metric)
# plt.plot(r, metric)
# plt.xlabel("Radius")
# plt.ylabel("Weighted Electric Field Magnitude (Normalized) at Bottom of NPF")
# plt.show()

# Plot the potential axially along r=0
plt.plot(z,U[:,0])
plt.xlabel('z along r=0')
plt.ylabel('Potential')
plt.ylim([-0.5,0.5])
plt.show()

# Plot the potential axially along r=0
Etot = np.sqrt(Ez**2 + Er**2)
plt.plot(z,Etot[:,0])
plt.xlabel('z along r=0')
plt.ylabel('Force')
plt.show()
plt.semilogy(z,Etot[:,0])
Ap = np.pi * rexit**2.
Ac = np.pi * rcavi**2.
plt.axhline(Etot[0,0]*Ap/Ac,color='k')
plt.xlabel('z along r=0')
plt.ylabel('Force')
plt.show()
