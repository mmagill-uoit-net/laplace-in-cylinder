import numpy as np
import matplotlib.pyplot as plt



N=64
A = np.loadtxt("data/soln_%03d.dat"%N)
A = A.reshape([N,N])

plt.pcolor(A,cmap='flag',vmin=0,vmax=2)
plt.colorbar()
plt.show()

