#include "laplace-in-cylinder.h"


int main(int argc, char* argv[]) {

  ////////////////////////////////////////////////////////////////
  /*
  //// Define domain, mesh, CLI
  if ( argc != 7 ) { printf("%s [rante] [zante] [rpore] [tpore] [Nr] [Nz] \n",argv[0]); return 1; }
  double rante = strtof(argv[1],NULL);     // Radius of antechamber
  double zante = strtof(argv[2],NULL);     // Width of antechamber
  double rpore = strtof(argv[3],NULL);    // nm
  double tpore = strtof(argv[4],NULL);    // Half-width of the pore
  double rmax = rante;
  double zmax = zante + tpore;
  int Nr = strtol(argv[5],NULL,10);       // Number of r nodes
  int Nz = strtol(argv[6],NULL,10);       // Number of z nodes
  ////
  */
  float rmax = 1.;
  float zmax = 1.;
  int Nr = 32;
  int Nz = 32;
  ////////////////////////////////////////////////////////////////
  //// DON'T CHANGE THIS PART
  //// Set up A,f,u and initialize w/o BCs
  // Initialize operator A, RHS vector F, and solution U
  double** A = malloc(sizeof(double*)*Nz*Nr); 
  double* f = malloc(sizeof(double)*Nz*Nr); 
  double* u = malloc(sizeof(double)*Nz*Nr);
  for (int i=0; i<Nz*Nr; i++) {
    A[i] = malloc(sizeof(double)*Nz*Nr);
    for (int j=0; j<Nz*Nr; j++) { A[i][j] = 0; }
    f[i] = 0; u[i] = 0;
  }
  // R mesh
  double* dr = malloc(sizeof(double)*(Nr-1));
  for (int i=0; i<Nr-1; i++) { dr[i] = rmax/(Nr-1); }
  // Z mesh
  double* dz = malloc(sizeof(double)*(Nz-1));
  for (int i=0; i<Nz-1; i++) { dz[i] = zmax/(Nz-1); }
  // Fill operator, ignoring boundaries
  FillOperatorWoBcs(A,Nz,Nr,dz,dr);
  ////

  ////////////////////////////////////////////////////////////////
  //// Define geometry, BCs
  //// All conditions are homogeneous by default
  //// Be careful about applying BCs in the same row twice
  //// New BCs wil override previous BCs

  // Indices
  int k;

  // Apply Homogeneous Newmann BCs at r=0
  // ALWAYS NECESSARY BY MATH
  for (int i=0; i<Nz; i++) {
    k = i*Nr + 0;
    SetBC_NewmannR(A,Nz,Nr,k,dr,+1);
    f[k] = 0.;
  }

  // Apply Newmann BCs at r=rmax
  // INSULATED WALLS OF CYLINDER
  for (int i=0; i<Nz; i++) {
    k = i*Nr + (Nr-1);
    SetBC_NewmannR(A,Nz,Nr,k,dr,-1);
    f[k] = 0.;
  }

  // Apply Newmann BCs at bottom of cylinder
  // INSULATED WALL OF CYLINDER'S BASE
  for (int j=1; j<Nr-1; j++) {
    k = 0*Nr + j;
    SetBC_NewmannZ(A,Nz,Nr,k,dz,+1);
    f[k] = 0.;
  }

  // Apply Newmann BCs at top of cylinder
  // INSULATED WALL OF CYLINDER'S TOP
  for (int j=1; j<Nr-1; j++) {
    k = (Nz-1)*Nr + j;
    SetBC_NewmannZ(A,Nz,Nr,k,dr,-1);
    f[k] = 0.;
  }

  // Override
  // Apply Dirichlet BCs at bottom of cylinder
  // BOTTOM NANOPORE AT FIXED POTENTIAL
  for (int j=1; j<5; j++) {
    k = 0*Nr + j;
    SetBC_Dirichlet(A,Nz,Nr,k);
    f[k] = -1.;
  }

  // Override
  // Apply Dirichlet BCs at top of cylinder
  // TOP NANOPORE AT FIXED POTENTIAL
  for (int j=1; j<5; j++) {
    k = (Nz-1)*Nr + j;
    SetBC_Dirichlet(A,Nz,Nr,k);
    f[k] = 1.;
  }

  ////
  ////////////////////////////////////////////////////////////////

  //// DON'T CHANGE THIS PART
  //// Solve, output, exit
  // Solve using LAPACK
  SolveSystem(A,u,f,Nz,Nr);
  // Output
  //  char fout_name[256]; sprintf(fout_name,"data/soln_%.1f_%.1f_%.1f_%.1f_%03d_%03d.dat",
  char fout_name[256]; sprintf(fout_name,"data/soln.dat");
  FILE* fout =fopen(fout_name,"w");
  for (int i=0; i<Nz*Nr; i++) {
    fprintf(fout,"%.8e\n",u[i]);
  }
  // Release Memory
  fclose(fout); free(f); free(u); 
  for (int i=0; i<Nz*Nr; i++) {
    free(A[i]);
  } free(A);
  // Exit
  return 0;
  ////
}
