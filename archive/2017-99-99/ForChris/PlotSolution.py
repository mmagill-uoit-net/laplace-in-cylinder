import sys

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

# Initialize figure
fig = plt.figure()
ax = plt.axes()

# Select case
N = int(sys.argv[1])
Nr = N
Nz = N

# Coordinates used in the field solver geometry
rmax = 16.6
tpore = 3.0
zmax = rmax+tpore


# Load data
u = np.loadtxt('data/soln_%03d.dat'%(N))
U = np.resize(u,(Nz,Nr))

# Coordinates
r = np.linspace(0,rmax,Nr)
z = np.linspace(0,zmax,Nz)
dr = r[1] - r[0]
dz = z[1] - z[0]
R = np.outer(np.ones(Nz),r)
Z = np.outer(z,np.ones(Nr))
DR = np.outer(np.ones(Nz),dr)
DZ = np.outer(dz,np.ones(Nr))

# Calculate gradient (cylindrical coordinates with symmetry means this is okay)
Ez,Er = np.gradient(-U,dz,dr)

# Check the unmodified solution
#ax.pcolor(R,Z,U,vmin=-1.,vmax=1.)
#plt.show()

# Unfold z anti-symmetry
UU = np.concatenate((U,-np.flipud(U[:-1,:])),axis=0)
RR = np.concatenate((R,R[:-1,:]),axis=0)
ZZ = np.concatenate((Z,zmax+Z[:-1,:]),axis=0)
#EEr = np.concatenate((Er,-np.flipud(Er)),axis=0)
#EEz = np.concatenate((Ez,np.flipud(Ez)),axis=0)
EEz,EEr = np.gradient(-UU,dz,dr)

# Plot
ax.pcolor(RR,ZZ,UU,vmin=-0.5,vmax=0.5)
ax.grid()
#V = np.linspace(0.9,1.0,30)
#V = np.concatenate((-V[::-1],V),axis=1)
#ax.contour(RR,ZZ,UU,V,colors='k',lw=6)
#ax.contour(R,Z,U,V,colors='k',lw=6)

#Streamplot doesn't work on uneven grids
ax.streamplot(RR,ZZ,EEr,EEz)

# Plot
plt.xlim([0,rmax])
plt.ylim([0,2*zmax])
plt.show()


