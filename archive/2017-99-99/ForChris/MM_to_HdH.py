import sys

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D


# Select case
N = int(sys.argv[1])
Nr = N
Nz = N

# Coordinates used in the field solver geometry
rmax = 16.6
tpore = 3.0
zmax = rmax+tpore


# Load data
u = np.loadtxt('data/soln_%03d.dat'%(N))
U = np.resize(u,(Nz,Nr))
UU = np.concatenate((np.fliplr(U),U),axis=1)
UU = np.concatenate((U,np.flipud(U)),axis=0)


# Coordinates
r = np.linspace(0,rmax,Nr)
z = np.linspace(0,zmax,Nz)
dr = r[1] - r[0]
dz = z[1] - z[0]
R = np.outer(np.ones(Nz),r)
Z = np.outer(z,np.ones(Nr))
DR = np.outer(np.ones(Nz),dr)
DZ = np.outer(dz,np.ones(Nr))

# Calculate gradient (cylindrical coordinates with symmetry means this is okay)
Ez,Er = np.gradient(-U,dz,dr)


# Unfold z anti-symmetry
UU = np.concatenate((U,-np.flipud(U[:-1,:])),axis=0)
RR = np.concatenate((R,R[:-1,:]),axis=0)
ZZ = np.concatenate((Z,zmax+Z[:-1,:]),axis=0)
#EEr = np.concatenate((Er,-np.flipud(Er)),axis=0)
#EEz = np.concatenate((Ez,np.flipud(Ez)),axis=0)
EEz,EEr = np.gradient(-UU,dz,dr)

# Lose a z mesh point to the symmetry
# DO NOT lose a mesh point to gradient, bc np.gradient is awsum.
Nz = 2*Nz - 1

# Reshape and output as a table for Espresso loader
outsize = Nr*Nz
with open('HDH_soln_%03d.dat'%N,'w') as outfile:
    for iz in range(Nz):
        for ir in range(Nr):
            k = ir*Nr + iz
            outfile.write( "%d %d %e %e\n"%(ir,iz,EEr[iz][ir],EEz[iz][ir]))
