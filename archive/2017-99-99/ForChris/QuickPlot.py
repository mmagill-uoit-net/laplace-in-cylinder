import numpy as np
import matplotlib.pyplot as plt



N=128
A = np.loadtxt("data/soln_%d.dat"%N)
A = A.reshape([N,N])

plt.pcolor(A,cmap='flag',vmin=0,vmax=2)
plt.colorbar()
plt.show()

