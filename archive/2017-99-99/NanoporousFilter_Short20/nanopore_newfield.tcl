#
# Inputs are, in order: 
# ratio of nanopore to cavity (I keep this as 3 for now)
# Force scaling factor
# Particle radius
# Seed
# Series (this number doesn't matter for now.)


set name "nanopore_v2"
set ident "_s4"



# setting number of particles, time step, skin
set n_part		100.0
set time_step	0.01
set skin		0.1
set vmd_output	"yes"
# pore radius
set rad1		5.7
# cavity radius
set rati_in 		[lindex $argv 0]
set rati		[expr $rati_in.0]
set rad2		[expr $rati*$rad1]
# pore depth
set leng1		1.0
# cavity depth
set leng2		$rad2
# forces
set Fcav		[lindex $argv 1]
# Particle radius
set rin			[lindex $argv 2]
set r			[expr 1.0 + $rin.0*0.2]
# random seed
set rseed		[lindex $argv 3]
# series number
set seri		[lindex $argv 4]

# prepare file to write data to
set nFcav [expr { int ($Fcav*100)}]
set nr [expr { int ($rin*100)}]
#set fdat [open [join [list np_F $nFcav _R $nr _ $seri .txt] {}] "w"]



t_random seed $rseed
set first [expr srand($rseed)]


# preparing the box
set box_l		[expr 1000.0]
set center		[expr $box_l/2.0]

# setting the system
setmd box_l			$box_l $box_l $box_l
setmd periodic		0 0 0
setmd time_step		$time_step
setmd skin			$skin
thermostat langevin 1.0 1.0

# setting initial position of particles
set n 		0
set part_id 0
while { $n < $n_part } {
	
	set posx		$center
	set posy		$center
	set posz		$center
	part $part_id pos $posx $posy $posz type 1 ext_force [expr $r*$Fcav] 1 1
	set time($n) 	0
	set n 			[expr $n + 1]
	set part_id 	[expr $part_id+1]
	

}
# Defining a cylindrical pore as a constraint
constraint pore center $center $center [expr $center+$leng1+$leng2] axis 0 0 1 radius $rad2 length $leng2 type 0
constraint pore center $center $center [expr $center+2*$leng1+2*$leng2] axis 0 0 1 radius $rad1 length $leng1 type 0
constraint pore center $center $center $center axis 0 0 1 radius $rad1 length $leng1 type 0

part 100 pos $center $center $center fix 1 1 1 type 2
part 101 pos $center $center [expr $center + 3*$leng1 +2*$leng2] fix 1 1 1 type 2


# Marking the walls
# Cavity walls
# Top
#part 100 pos [expr $center+$rad2] [expr $center+$leng1+2.0*$leng2] $center fix 1 1 1 type 2
#part 101 pos [expr $center-$rad2] [expr $center+$leng1+2.0*$leng2] $center fix 1 1 1 type 2
#part 102 pos $center [expr $center+$leng1+2.0*$leng2] [expr $center+$rad2] fix 1 1 1 type 2
#part 103 pos $center [expr $center+$leng1+2.0*$leng2] [expr $center-$rad2] fix 1 1 1 type 2
#part 104 pos [expr $center+$rad2/1.414213562] [expr $center+$leng1+2.0*$leng2] [expr $center+$rad2/1.414213562] fix 1 1 1 type 2
#part 105 pos [expr $center+$rad2/1.414213562] [expr $center+$leng1+2.0*$leng2] [expr $center-$rad2/1.414213562] fix 1 1 1 type 2
#part 106 pos [expr $center-$rad2/1.414213562] [expr $center+$leng1+2.0*$leng2] [expr $center+$rad2/1.414213562] fix 1 1 1 type 2
#part 107 pos [expr $center-$rad2/1.414213562] [expr $center+$leng1+2.0*$leng2] [expr $center-$rad2/1.414213562] fix 1 1 1 type 2
#bottom
#part 108 pos [expr $center+$rad2] [expr $center+$leng1] $center fix 1 1 1 type 2
#part 109 pos [expr $center-$rad2] [expr $center+$leng1] $center fix 1 1 1 type 2
#part 110 pos $center [expr $center+$leng1] [expr $center+$rad2] fix 1 1 1 type 2
#part 111 pos $center [expr $center+$leng1] [expr $center-$rad2] fix 1 1 1 type 2
#part 112 pos [expr $center+$rad2/1.414213562] [expr $center+$leng1] [expr $center+$rad2/1.414213562] fix 1 1 1 type 2
#part 113 pos [expr $center+$rad2/1.414213562] [expr $center+$leng1] [expr $center-$rad2/1.414213562] fix 1 1 1 type 2
#part 114 pos [expr $center-$rad2/1.414213562] [expr $center+$leng1] [expr $center+$rad2/1.414213562] fix 1 1 1 type 2
#part 115 pos [expr $center-$rad2/1.414213562] [expr $center+$leng1] [expr $center-$rad2/1.414213562] fix 1 1 1 type 2
#middle
#part 116 pos [expr $center+$rad2] [expr $center+$leng1+$leng2] $center fix 1 1 1 type 2
#part 117 pos [expr $center-$rad2] [expr $center+$leng1+$leng2] $center fix 1 1 1 type 2
#part 118 pos $center [expr $center+$leng1+$leng2] [expr $center+$rad2] fix 1 1 1 type 2
#part 119 pos $center [expr $center+$leng1+$leng2] [expr $center-$rad2] fix 1 1 1 type 2
#part 120 pos [expr $center+$rad2/1.414213562] [expr $center+$leng1+$leng2] [expr $center+$rad2/1.414213562] fix 1 1 1 type 2
#part 121 pos [expr $center+$rad2/1.414213562] [expr $center+$leng1+$leng2] [expr $center-$rad2/1.414213562] fix 1 1 1 type 2
#part 122 pos [expr $center-$rad2/1.414213562] [expr $center+$leng1+$leng2] [expr $center+$rad2/1.414213562] fix 1 1 1 type 2
#part 123 pos [expr $center-$rad2/1.414213562] [expr $center+$leng1+$leng2] [expr $center-$rad2/1.414213562] fix 1 1 1 type 2

# Cis nanopore
# Outer
#part 124 pos [expr $center+$rad1] [expr $center-$leng1] $center fix 1 1 1 type 3
#part 125 pos [expr $center-$rad1] [expr $center-$leng1] $center fix 1 1 1 type 3
#part 126 pos $center [expr $center-$leng1] [expr $center+$rad1] fix 1 1 1 type 3
#part 127 pos $center [expr $center-$leng1] [expr $center-$rad1] fix 1 1 1 type 3
#part 128 pos [expr $center+$rad1/1.414213562] [expr $center-$leng1] [expr $center+$rad1/1.414213562] fix 1 1 1 type 3
#part 129 pos [expr $center+$rad1/1.414213562] [expr $center-$leng1] [expr $center-$rad1/1.414213562] fix 1 1 1 type 3
#part 130 pos [expr $center-$rad1/1.414213562] [expr $center-$leng1] [expr $center+$rad1/1.414213562] fix 1 1 1 type 3
#part 131 pos [expr $center-$rad1/1.414213562] [expr $center-$leng1] [expr $center-$rad1/1.414213562] fix 1 1 1 type 3
# Inner
#part 132 pos [expr $center+$rad1] [expr $center+$leng1] $center fix 1 1 1 type 3
#part 133 pos [expr $center-$rad1] [expr $center+$leng1] $center fix 1 1 1 type 3
#part 134 pos $center [expr $center+$leng1] [expr $center+$rad1] fix 1 1 1 type 3
#part 135 pos $center [expr $center+$leng1] [expr $center-$rad1] fix 1 1 1 type 3
#part 136 pos [expr $center+$rad1/1.414213562] [expr $center+$leng1] [expr $center+$rad1/1.414213562] fix 1 1 1 type 3
#part 137 pos [expr $center+$rad1/1.414213562] [expr $center+$leng1] [expr $center-$rad1/1.414213562] fix 1 1 1 type 3
#part 138 pos [expr $center-$rad1/1.414213562] [expr $center+$leng1] [expr $center+$rad1/1.414213562] fix 1 1 1 type 3
#part 139 pos [expr $center-$rad1/1.414213562] [expr $center+$leng1] [expr $center-$rad1/1.414213562] fix 1 1 1 type 3

# Trans nanopore
# Outer
#part 140 pos [expr $center+$rad1] [expr $center+3*$leng1+2*$leng2] $center fix 1 1 1 type 3
#part 141 pos [expr $center-$rad1] [expr $center+3*$leng1+2*$leng2] $center fix 1 1 1 type 3
#part 142 pos $center [expr $center+3*$leng1+2*$leng2] [expr $center+$rad1] fix 1 1 1 type 3
#part 143 pos $center [expr $center+3*$leng1+2*$leng2] [expr $center-$rad1] fix 1 1 1 type 3
#part 144 pos [expr $center+$rad1/1.414213562] [expr $center+3*$leng1+2*$leng2] [expr $center+$rad1/1.414213562] fix 1 1 1 type 3
#part 145 pos [expr $center+$rad1/1.414213562] [expr $center+3*$leng1+2*$leng2] [expr $center-$rad1/1.414213562] fix 1 1 1 type 3
#part 146 pos [expr $center-$rad1/1.414213562] [expr $center+3*$leng1+2*$leng2] [expr $center+$rad1/1.414213562] fix 1 1 1 type 3
#part 147 pos [expr $center-$rad1/1.414213562] [expr $center+3*$leng1+2*$leng2] [expr $center-$rad1/1.414213562] fix 1 1 1 type 3
# Inner
#part 148 pos [expr $center+$rad1] [expr $center+$leng1+2*$leng2] $center fix 1 1 1 type 3
#part 149 pos [expr $center-$rad1] [expr $center+$leng1+2*$leng2] $center fix 1 1 1 type 3
#part 150 pos $center [expr $center+$leng1+2*$leng2] [expr $center+$rad1] fix 1 1 1 type 3
#part 151 pos $center [expr $center+$leng1+2*$leng2] [expr $center-$rad1] fix 1 1 1 type 3
#part 152 pos [expr $center+$rad1/1.414213562] [expr $center+$leng1+2*$leng2] [expr $center+$rad1/1.414213562] fix 1 1 1 type 3
#part 153 pos [expr $center+$rad1/1.414213562] [expr $center+$leng1+2*$leng2] [expr $center-$rad1/1.414213562] fix 1 1 1 type 3
#part 154 pos [expr $center-$rad1/1.414213562] [expr $center+$leng1+2*$leng2] [expr $center+$rad1/1.414213562] fix 1 1 1 type 3
#part 155 pos [expr $center-$rad1/1.414213562] [expr $center+$leng1+2*$leng2] [expr $center-$rad1/1.414213562] fix 1 1 1 type 3
# Legend for particle radius scale
#part 156 pos [expr $center+30] [expr $center+20] $center fix 1 1 1 type 4
#part 157 pos [expr $center+30] [expr $center+20.0+$r] $center fix 1 1 1 type 4





# preparing connection
if { $vmd_output == "yes" } {
	prepare_vmd_connection hundred_particles 30000
	exec sleep 4
	imd positions
}





# sigma is the average of the wall radius and the particle radius, with the wall radius being 1 and the 
# particle radius starting from 0.5 and going up to 5
set sig [expr (0.5 + $r)]
# defining the interaction between the pore and the particles
inter 0 1 lennard-jones 1.0 $sig $sig 0.25 0.0
# capping the force
inter ljforcecap 200
thermostat langevin 1.0 $r


# i keeps track of the time passed. passed keeps track of the number of particles that have 
# succesfully passed through the nanopore. time is where the translocation times will be summed 
# to find the average
set i 0
set passed 0
set t 0


# n_cycle represents the number of particles still in the cavity, it will integrate untill this is zero
set n_cycle $n_part

# main loop
while { $n_cycle > 0 } {
	#puts "$i $r"

	for { set j 0 } { $j < $n_part } { incr j } {

		# getting the positions
		set x [lindex [part $j print pos] 0]
		set y [lindex [part $j print pos] 1]
		set z [lindex [part $j print pos] 2]



		# this if statement keeps it from recording the particles that have already passed through, 
		# or have gone out the bottom more than once. The particles we no longer care about.
		if {$z < [expr -50]} {
			part $j pos $x $y $z ext_force 0.0 0.0 0.0
		} else { 

			# This is the meat, the particles we do care about.
			# This first if checks to see if the particle has gone out the cis side, and if it has, 
			# it teleports it back to the start and restarts its timer
			if {$z < [expr $center-$leng1-1]} {
				part $j pos $posx $posy $posz
				set time($j)	0
				#puts "oops"
				
			} else {


							if {$z > [expr $center + 3*$leng1 +2*$leng2]} {
								set t [expr $time($j)*$time_step]
								set tele [expr -999]
								part $j pos $tele $tele $tele fix $tele $tele $tele
								incr passed
								puts "bloop, number $passed"
								puts "time $t"
								set n_cycle [expr $n_cycle - 1]
								#puts $fdat "$t"


				} else { }
			}
		}
		# increase the current particle's time
		set time($j) [expr $time($j) + 1]
	}
	if { $vmd_output == "yes" } {imd positions}
	integrate [expr int(1)]	
	incr i


		

	
}
set avgt [expr $t/$passed]

