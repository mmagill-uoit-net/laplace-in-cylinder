import sys

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib.colors import LogNorm

# Initialize figure
fig = plt.figure()
ax = plt.axes()
#ax = fig.add_subplot(111, projection='3d')

# Select case
N = int(sys.argv[1])
Nr = N
Nz = N

# Coordinates used in the field solver geometry
sigma = 5.
rmax = 500./sigma
zmax = (200.+20.)/sigma
rexit = 1.3 # *sigma
rcavi = 40. # *sigma

# Load data
u = np.loadtxt('data/soln_%03d.dat'%(N))
U = np.resize(u,(Nz,Nr))

# Rescale potential
U = U - U.min()
U = U * 3.848 * 3.3
real_vmax = U[np.where(U<U.mean())].max() # Ignore walls
U[np.where(U>U.mean())] = 0 # Flip walls
U = U + 1e-9

# Coordinates
r = np.linspace(0,rmax,Nr)
z = np.linspace(0,zmax,Nz)
dr = r[1] - r[0]
dz = z[1] - z[0]
R = np.outer(np.ones(Nz),r)
Z = np.outer(z,np.ones(Nr))
DR = np.outer(np.ones(Nz),dr)
DZ = np.outer(dz,np.ones(Nr))

# Calculate gradient (cylindrical coordinates with symmetry means this is okay)
Ez,Er = np.gradient(-U,dz,dr)
E = np.sqrt(Ez**2 + Er**2)

# Check the unmodified solution (no z symmetry in this system)
#cax = ax.pcolor(R,Z,U,vmin=-1.,vmax=1.,cmap='flag')
#cax = ax.pcolor(R,Z,np.log(U+1.),vmin=0,vmax=np.log(real_vmax),cmap='spring')
#cax = ax.pcolor(R,Z,np.log(U),vmin=-10,vmax=3,cmap='spring')
#cbar = plt.colorbar(cax,ax=ax)
plt.pcolor(R, Z, U, norm=LogNorm(vmin=U[np.where(U>2e-9)].min(), vmax=U.max()), cmap='coolwarm')
plt.colorbar(orientation='horizontal',label=r'Electric Potential Energy per Monomer ($k_{\mathrm{B}}T$)')
ax.set_aspect('equal')
#plt.axes().set_aspect('equal', 'datalim')
#cf = ax.contourf(R, Z, U, np.linspace(0, real_vmax, 1000.),
#                 extend='both')
#cbar = plt.colorbar(cf,)
ax.streamplot(R,Z,Er,Ez)
ax.set_xlabel(r"Lateral Position ($\sigma$)")
ax.set_ylabel(r"Axial Position ($\sigma$)")

# ## Plot E field as a surface
# #ax.plot_surface(R,Z,E)
# #ax.set_zlim(0,0.0005)

# Plot
plt.xlim([0,rmax])
plt.ylim([0,zmax])
#plt.show()
plt.savefig("field13.png",dpi=300)

# # Plot abs(E)(r) at z = zmax
# Er_top = Er[-1,:]
# Ez_top = Ez[-1,:]
# E_top = np.sqrt(Er_top**2 + Ez_top**2)
# plt.plot(r, E_top/E_top[0])
# plt.xlabel("Radius")
# plt.ylabel("Normalized Electric Field Magnitude at Bottom of NPF")
# plt.show()

# # Plot r*abs(E)(r) at z = zmax (crude estimate of capture prob)
# Er_top = Er[-1,:]
# Ez_top = Ez[-1,:]
# E_top = np.sqrt(Er_top**2 + Ez_top**2)
# metric = r*E_top
# metric = metric / np.max(metric)
# plt.plot(r, metric)
# plt.xlabel("Radius")
# plt.ylabel("Weighted Electric Field Magnitude (Normalized) at Bottom of NPF")
# plt.show()

# # Plot the potential axially along r=0
# plt.plot(z,U[:,0])
# plt.xlabel('z along r=0')
# plt.ylabel('Potential')
# plt.ylim([-0.5,0.5])
# plt.show()

# # Plot the potential axially along r=0
# Etot = np.sqrt(Ez**2 + Er**2)
# plt.plot(z,Etot[:,0])
# plt.xlabel('z along r=0')
# plt.ylabel('Force')
# plt.show()
# plt.semilogy(z,Etot[:,0])
# Ap = np.pi * rexit**2.
# Ac = np.pi * rcavi**2.
# plt.axhline(Etot[0,0]*Ap/Ac,color='k')
# plt.xlabel('z along r=0')
# plt.ylabel('Force')
# plt.show()
