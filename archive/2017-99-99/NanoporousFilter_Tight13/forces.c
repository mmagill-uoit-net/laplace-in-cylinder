// This file is part of the ESPResSo distribution (http://www.espresso.mpg.de).
// It is therefore subject to the ESPResSo license agreement which you accepted upon receiving the distribution
// and by which you are legally bound while utilizing this file in any form or way.
// There is NO WARRANTY, not even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// You should have received a copy of that license along with this program;
// if not, refer to http://www.espresso.mpg.de/license.html where its current version can be found, or
// write to Max-Planck-Institute for Polymer Research, Theory Group, PO Box 3148, 55021 Mainz, Germany.
// Copyright (c) 2002-2005; all rights reserved unless otherwise stated.
/** \file forces.c Force calculation.
 *
 *  For more information see \ref forces.h "forces.h".
*/
#include <mpi.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "utils.h"
#include "thermostat.h"
#include "pressure.h"
#include "communication.h"
#include "ghosts.h" 
#include "verlet.h"
#include "grid.h"
#include "cells.h"
#include "particle_data.h"
#include "interaction_data.h"
#include "rotation.h"
#include "forces.h"
#include "elc.h"
#include "lattice.h"
#include "lb.h"
#include "nsquare.h"
#include "layered.h"
#include "domain_decomposition.h"
#include "magnetic_non_p3m__methods.h"
#include "mdlc_correction.h"

/************************************************************/
/* local prototypes                                         */
/************************************************************/

/** Calculate long range forces (P3M, MMM2d...). */
void calc_long_range_forces();

/** initialize real particle forces with thermostat forces and
    ghost particle forces with zero. */
void init_forces();

/************************************************************/

void force_calc()
{
  

#if defined(DIPOLES) && defined(ROTATION)
  convert_quat_to_dip_all();
#endif

  init_forces();
  switch (cell_structure.type) {
  case CELL_STRUCTURE_LAYERED:
    layered_calculate_ia();
    break;
  case CELL_STRUCTURE_DOMDEC:
    if(dd.use_vList) {
      if (rebuild_verletlist)
	build_verlet_lists_and_calc_verlet_ia();
      else
	calculate_verlet_ia();
    }
    else
      calc_link_cell();
    break;
  case CELL_STRUCTURE_NSQUARE:
    nsq_calculate_ia();
    
  }

  calc_long_range_forces();

#ifdef LB
  if (lattice_switch & LATTICE_LB) calc_particle_lattice_ia() ;
#endif

#ifdef COMFORCE
  calc_comforce();
#endif

/* this must be the last force to be calculated (Mehmet)*/
#ifdef COMFIXED
  calc_comfixed();
#endif

}

/************************************************************/

void calc_long_range_forces()
{
#ifdef ELECTROSTATICS  
  /* calculate k-space part of electrostatic interaction. */
  switch (coulomb.method) {
#ifdef ELP3M
  case COULOMB_ELC_P3M:
    if (elc_params.dielectric_contrast_on) {
      ELC_P3M_modify_p3m_sums_both();
      ELC_P3M_charge_assign_both();
      ELC_P3M_self_forces();
    }
    else
      P3M_charge_assign();

    P3M_calc_kspace_forces_for_charges(1,0);

    if (elc_params.dielectric_contrast_on)
      ELC_P3M_restore_p3m_sums();
 
    ELC_add_force(); 

    break;
  case COULOMB_P3M:
    P3M_charge_assign();
#ifdef NPT
    if(integ_switch == INTEG_METHOD_NPT_ISO)
      nptiso.p_vir[0] += P3M_calc_kspace_forces_for_charges(1,1);
    else
#endif
      P3M_calc_kspace_forces_for_charges(1,0);
    break;
#endif
  case COULOMB_EWALD:
#ifdef NPT
    if(integ_switch == INTEG_METHOD_NPT_ISO)
      nptiso.p_vir[0] += EWALD_calc_kspace_forces(1,1);
    else
#endif
      EWALD_calc_kspace_forces(1,0);
    break;
  case COULOMB_MAGGS:
    maggs_calc_e_forces();
    break;
  case COULOMB_MMM2D:
    MMM2D_add_far_force();
    MMM2D_dielectric_layers_force_contribution();
  }
#endif  /*ifdef ELECTROSTATICS */

#ifdef MAGNETOSTATICS  
  /* calculate k-space part of the magnetostatic interaction. */
  switch (coulomb.Dmethod) {
#ifdef ELP3M
#ifdef MDLC
  case DIPOLAR_MDLC_P3M:
     add_mdlc_force_corrections();
    //fall through 
#endif
  case DIPOLAR_P3M:
    P3M_dipole_assign();
#ifdef NPT
    if(integ_switch == INTEG_METHOD_NPT_ISO) {
      nptiso.p_vir[0] += P3M_calc_kspace_forces_for_dipoles(1,1);
      fprintf(stderr,"dipolar_P3M at this moment is added to p_vir[0]\n");    
    } else
#endif
      P3M_calc_kspace_forces_for_dipoles(1,0);

      break;
#endif
#ifdef DAWAANR
  case DIPOLAR_ALL_WITH_ALL_AND_NO_REPLICA: 
      dawaanr_calculations(1,0);
      break;
#endif
#ifdef MAGNETIC_DIPOLAR_DIRECT_SUM
#ifdef MDLC
  case DIPOLAR_MDLC_DS:
     add_mdlc_force_corrections();
    //fall through 
#endif
  case DIPOLAR_DS: 
        magnetic_dipolar_direct_sum_calculations(1,0);
      break;
#endif

  }
#endif  /*ifdef MAGNETOSTATICS */
}

int ext_loaded=0;
/************************************************************/

/** initialize the forces for a real particle */
MDINLINE void init_local_particle_force(Particle *part)
{
  if ( thermo_switch & THERMO_LANGEVIN )
    friction_thermo_langevin(part);
  else {
    part->f.f[0] = 0;
    part->f.f[1] = 0;
    part->f.f[2] = 0;
  }

	
  // Arrays to store field r and z components
  float frArray[1001][1001];
  float fzArray[1001][1001];

  // Object to hold opened file
  FILE *fForceFile;

  // Only load the file once per simulation
  if (ext_loaded==0){

    // Open the field file
    fForceFile=fopen("HDH_soln_128.dat", "r");

    // Variables for the current position vector (integer form)
    int rr,rz;
    // Variables for the current field vector
    float rFr,rFz;

    // Read the file and fill the arrays
    while(!feof(fForceFile)){
      fscanf(fForceFile,"%i %i %e %e",&rr,&rz,&rFr,&rFz);
      if(!feof(fForceFile)){
	frArray[rr][rz]=rFr;
	fzArray[rr][rz]=rFz;
      }

    }
    
    // Confirm field is read
    printf("Field was read!\n");
    /*
    int i; int j;
    for (i=0; i<10; i++){
      for (j=0; j<10; j++){
	printf("%d %d %e %e\n",i,j,frArray[i][j],fzArray[i][j]);
      }
    }
    */

    // Ensure field is only read once per simulation
    ext_loaded=1;
  }
	
#ifdef EXTERNAL_FORCES   
  if(part->l.ext_flag & PARTICLE_EXT_FORCE) {
    
    // Particle's current position (integer form)
    int ir,iz;
    
    // Geometry used in computing the field
    // Exploits symmetry in r and antisymmetry in z
    float sigma = 5.;
    float rmax = 500./sigma;       // Radius of antechamber
    float tante = 200./sigma;      // Thickness of antechamber
    float tpore = 20./sigma;       // Thickness of exit pore
    float zmax = tante+tpore;      // From bottom of exit pore to bottom of npf
    int N = 180;
    int Nr = N;
    int Nz = N;

    // Mesh spacing
    float dr = rmax/(Nr-1);
    float dz = zmax/(Nz-1);
    
    // Force vector
    float fx=0; float fy=0; float fz=0;
    
    // Coordinates of the nanopore
    float cx=100; float cy=100; 
    float cz=200-(tpore/2.); // z center is at middle of lower pore
    
    // Current particle position
    float rx=part->r.p[0];
    float ry=part->r.p[1];
    float rz=part->r.p[2];
    
    // Current particle position relative to the nanopore
    float r=sqrt( (cx-rx)*(cx-rx) +  (cy-ry)*(cy-ry) );
    float costheta,sintheta;
    if(r!=0){
      costheta=((rx-cx)/r);
      sintheta=((ry-cy)/r);
    }
    else{
      costheta=0;
      sintheta=0;
    }
    rz=rz-cz;
    
    // Go to the folded coordinates
    //while (rx>box_l[0]) rx-=box_l[0];
    //while (rx<0) rx+=box_l[0];

    // Apply numerical force to particle 
    if(r < rmax && rz < zmax && rz > 0){
      // Find the particle's position
      // Currently round position to nearest mesh point
      // Should someday improve to interpolate b/w mesh points
      ir = (int)round(r/dr);
      iz = (int)round(rz/dz);

      // Convert field from polar coords to cartesian
      if(rz!=0){
	fx=frArray[ir][iz]*costheta;
	fy=frArray[ir][iz]*sintheta;
	//	printf("ir = %d, iz = %d, fr = %e\n",ir,iz,frArray[ir][iz]);
      } else {
	fx=0;
	fy=0;
      }
      fz=fzArray[ir][iz];
    } else {
      // Outside the cavity, set to zero for now (later bring in the theoretical field)
      fx=0; fy=0; fz=0;
    }
    // Scale the forces
    // The ext_force[0] variable has been hijacked for this purpose
    fx=part->l.ext_force[0]*fx;
    fy=part->l.ext_force[0]*fy;
    fz=part->l.ext_force[0]*fz;
    
    //printf("x: %f y: %f z: %f  r: %f dz: %f ct: %f st: %f  fr: %E fz: %E  fx: %f fy: %f fz: %f\n",rx,ry,rz, r,dz,costheta,sintheta, frArray[ir][iz], fzArray[ir][iz], fx, fy, fz);
    //getchar();
    part->f.f[0] += fx;
    part->f.f[1] += fy;
    part->f.f[2] += fz;
  }
#endif
  
#ifdef ROTATION
  {
    double scale;
    /* set torque to zero */
    part->f.torque[0] = 0;
    part->f.torque[1] = 0;
    part->f.torque[2] = 0;
    
    /* and rescale quaternion, so it is exactly of unit length */	
    scale = sqrt( SQR(part->r.quat[0]) + SQR(part->r.quat[1]) +
		  SQR(part->r.quat[2]) + SQR(part->r.quat[3]));
    part->r.quat[0]/= scale;
    part->r.quat[1]/= scale;
    part->r.quat[2]/= scale;
    part->r.quat[3]/= scale;
  }
#endif
}

/** initialize the forces for a ghost particle */
MDINLINE void init_ghost_force(Particle *part)
{
  part->f.f[0] = 0;
  part->f.f[1] = 0;
  part->f.f[2] = 0;

#ifdef ROTATION
  {
    double scale;
    /* set torque to zero */
    part->f.torque[0] = 0;
    part->f.torque[1] = 0;
    part->f.torque[2] = 0;

    /* and rescale quaternion, so it is exactly of unit length */	
    scale = sqrt( SQR(part->r.quat[0]) + SQR(part->r.quat[1]) +
		  SQR(part->r.quat[2]) + SQR(part->r.quat[3]));
    part->r.quat[0]/= scale;
    part->r.quat[1]/= scale;
    part->r.quat[2]/= scale;
    part->r.quat[3]/= scale;
  }
#endif
}

void init_forces()
{
  Cell *cell;
  Particle *p;
  int np, c, i;

  /* The force initialization depends on the used thermostat and the
     thermodynamic ensemble */

#ifdef NPT
  /* reset virial part of instantaneous pressure */
  if(integ_switch == INTEG_METHOD_NPT_ISO)
    nptiso.p_vir[0] = nptiso.p_vir[1] = nptiso.p_vir[2] = 0.0;
#endif


  /* initialize forces with langevin thermostat forces
     or zero depending on the thermostat
     set torque to zero for all and rescale quaternions
  */
  for (c = 0; c < local_cells.n; c++) {
    cell = local_cells.cell[c];
    p  = cell->part;
    np = cell->n;
    for (i = 0; i < np; i++)
      init_local_particle_force(&p[i]);
  }

  /* initialize ghost forces with zero
     set torque to zero for all and rescale quaternions
  */
  for (c = 0; c < ghost_cells.n; c++) {
    cell = ghost_cells.cell[c];
    p  = cell->part;
    np = cell->n;
    for (i = 0; i < np; i++)
      init_ghost_force(&p[i]);
  }
   
#ifdef CONSTRAINTS
  init_constraint_forces();
#endif
}

void init_forces_ghosts()
{
  Cell *cell;
  Particle *p;
  int np, c, i;

  for (c = 0; c < ghost_cells.n; c++) {
    cell = ghost_cells.cell[c];
    p  = cell->part;
    np = cell->n;
    for (i = 0; i < np; i++)
      init_ghost_force(&p[i]);
  }
}


