import sys

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

# Initialize figure
fig = plt.figure()
ax = plt.axes()
#ax = fig.add_subplot(111, projection='3d')

# Select case
N = int(sys.argv[1])
Nr = N
Nz = N

# Coordinates used in the field solver geometry
sigma = 5.
rmax = 500./sigma
zmax = (200.+20.)/sigma
rexit = 2.0 # *sigma
rcavi = 40. # *sigma

# Load data
u = np.loadtxt('laplace-in-cylinder/NanoporousFilter/data/soln_%03d.dat'%(N))
u2 = np.loadtxt('nanoporous-filter/field/data/soln_100.0_40.0_064_064.dat')
u2 = u2 + 0.5
U = np.resize(u,(Nz,Nr))
U2 = np.resize(u2,(64,64))

# Coordinates
r = np.linspace(0,rmax,Nr)
z = np.linspace(0,zmax,Nz)
dr = r[1] - r[0]
dz = z[1] - z[0]

r2 = np.linspace(0,rmax,64)
z2 = np.linspace(0,40.,64) + 4.
dr2 = r2[1] - r2[0]
dz2 = z2[1] - z2[0]

# Calculate gradient (cylindrical coordinates with symmetry means this is okay)
Ez,Er = np.gradient(-U,dz,dr)
E = np.sqrt(Ez**2 + Er**2)
Ez2,Er2 = np.gradient(-U2,dz2,dr2)
E2 = np.sqrt(Ez2**2 + Er2**2)

# Plot the potential axially along r=0
plt.plot(z,U[:,0],label='New, rexit=2')
plt.plot(z2,U2[:,0],label='Old, no pore')
plt.xlabel('z along r=0')
plt.ylabel('Potential')
plt.ylim([-0.55,0.55])
plt.legend()
plt.show()

# Plot the potential axially along r=0
plt.plot(z,E[:,0],label='New, rexit=2')
plt.plot(z2,E2[:,0],label='Old, no pore')
plt.xlabel('z along r=0')
plt.ylabel('Force')
plt.legend()
plt.show()
# plt.semilogy(z,E[:,0])
# plt.semilogy(z,E2[:,0])
# plt.xlabel('z along r=0')
# plt.ylabel('Force')
# plt.show()
