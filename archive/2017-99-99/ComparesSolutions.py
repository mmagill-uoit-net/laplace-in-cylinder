import sys

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

# Initialize figure
fig = plt.figure()
ax = plt.axes()
#ax = fig.add_subplot(111, projection='3d')

# Select case
N = int(sys.argv[1])
Nr = N
Nz = N

# Coordinates used in the field solver geometry
sigma = 5.
rmax = 500./sigma
zmax = (200.+20.)/sigma
rexit = 2.0 # *sigma
rcavi = 40. # *sigma

# Load data
u = np.loadtxt('NanoporousFilter/data/soln_%03d.dat'%(N))
u2 = np.loadtxt('NanoporousFilter_Tight13/data/soln_%03d.dat'%(N))
U = np.resize(u,(Nz,Nr))
U2 = np.resize(u2,(Nz,Nr))

# Coordinates
r = np.linspace(0,rmax,Nr)
z = np.linspace(0,zmax,Nz)
dr = r[1] - r[0]
dz = z[1] - z[0]
R = np.outer(np.ones(Nz),r)
Z = np.outer(z,np.ones(Nr))
DR = np.outer(np.ones(Nz),dr)
DZ = np.outer(dz,np.ones(Nr))

# Calculate gradient (cylindrical coordinates with symmetry means this is okay)
Ez,Er = np.gradient(-U,dz,dr)
E = np.sqrt(Ez**2 + Er**2)
Ez2,Er2 = np.gradient(-U2,dz,dr)
E2 = np.sqrt(Ez2**2 + Er2**2)

# Plot the potential axially along r=0
plt.plot(z,U[:,0])
plt.plot(z,U2[:,0])
plt.xlabel('z along r=0')
plt.ylabel('Potential')
plt.ylim([-0.5,0.5])
plt.show()

# Plot the potential axially along r=0
plt.plot(z,E[:,0])
plt.plot(z,E2[:,0])
plt.xlabel('z along r=0')
plt.ylabel('Force')
plt.show()
plt.semilogy(z,E[:,0])
plt.semilogy(z,E2[:,0])
plt.xlabel('z along r=0')
plt.ylabel('Force')
plt.show()
