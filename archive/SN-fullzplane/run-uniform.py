import os


resn = 1

#for rmax in [50,100]:
for rmax in [10,30]:

    zmax = 2*rmax
    rfine = rmax/10
    zfine = zmax/10

    Nr = rmax / resn
    Nz = 2*Nr

    Nrfine = Nr/10
    Nzfine = Nz/10
    Nrcoarse = Nr - Nrfine
    Nzcoarse = Nz - Nzfine

    print (rmax,zmax,rfine,zfine,Nrfine,Nzfine,Nrcoarse,Nzcoarse)

    os.system('time ./geometry.exe %f %f %f %f %d %d %d %d' %
              (rmax,zmax,rfine,zfine,Nrfine,Nzfine,Nrcoarse,Nzcoarse))
