#include "laplace-in-cylinder.h"


int main(int argc, char* argv[]) {

  ////////////////////////////////////////////////////////////////
  //// Define domain, mesh, CLI
  if ( argc != 3 ) { printf("%s [Nr] [rmax]\n",argv[0]); return 1; }
  int Nr = strtol(argv[1],NULL,10); 
  int Nz = 2*Nr;
  double rmax = strtof(argv[2],NULL);
  double zmax = 2.0*rmax;
  ////
  ////////////////////////////////////////////////////////////////

  //// DON'T CHANGE THIS PART
  //// Set up A,f,u and initialize w/o BCs
  // Initialize operator A, RHS vector F, and solution U
  double** A = malloc(sizeof(double*)*Nz*Nr); 
  double* f = malloc(sizeof(double)*Nz*Nr); 
  double* u = malloc(sizeof(double)*Nz*Nr);
  for (int i=0; i<Nz*Nr; i++) {
    A[i] = malloc(sizeof(double)*Nz*Nr);
    for (int j=0; j<Nz*Nr; j++) { A[i][j] = 0; }
    f[i] = 0; u[i] = 0;
  }
  // Fill operator, ignoring boundaries
  double dz = zmax / (double) Nz; double dr = rmax / (double) Nr;
  FillOperatorWoBcs(A,Nz,Nr,dz,dr);
  ////

  ////////////////////////////////////////////////////////////////
  //// Define geometry, BCs
  //// Be careful about applying BCs in the same row twice
  //// e.g. Newmann then Dirichlet w.o reinitializing is wrong
  //// All conditions are homogeneous by default

  // Geometric parameters
  double rpore = 1.0;
  int Nrpore = (int) floor(rpore/dr);
  double tpore = rpore;  // Half-width of the pore
  int Nzcis = (int) floor((Nz/2) - (tpore/dz));
  int Nztrans = (int) floor((Nz/2) + (tpore/dz));
  int k_z0; int k_zmax;
  int k_zcis; int k_ztrans;
  int k_r0; int k_rmax;
  int k_rpore; int k_inpore;

  // Apply homogenous Newmann BCs in r for (0<z<zmax,r=0)
  for (int i=1; i<Nz-1; i++) {
    k_r0 = i*Nr + 0;
    SetBC_NewmannR(A,Nz,Nr,k_r0,dr,+1);
  }

  // Apply non-homogeneous Dirichlet BCs for (z={0,zmax},all r)
  for (int j=0; j<Nr; j++) {
    k_z0 = 0*Nr + j;
    SetBC_Dirichlet(A,Nz,Nr,k_z0);
    f[k_z0] = 1.;
    k_zmax = (Nz-1)*Nr + j;
    SetBC_Dirichlet(A,Nz,Nr,k_zmax);
    f[k_zmax] = -1.;
  }

  // Apply homogenous Newmann BCs in r for (0<z<zcis,r=rmax)
  for (int i=1; i<Nzcis; i++) {
    k_rmax = i*Nr + (Nr-1);
    SetBC_NewmannR(A,Nz,Nr,k_rmax,dr,-1);
  }
  // Apply homogenous Newmann BCs in r for (ztrans<z<zmax,r=rmax)
  for (int i=Nztrans+1; i<Nz-1; i++) {
    k_rmax = i*Nr + (Nr-1);
    SetBC_NewmannR(A,Nz,Nr,k_rmax,dr,-1);
  }

  // Apply homogenous Newmann BCs in z for (z={zcis,ztrans},rpore<r<=rmax)
  for (int j=Nrpore+1; j<Nr; j++) {
    k_zcis = Nzcis*Nr + j;
    SetBC_NewmannZ(A,Nz,Nr,k_zcis,dz,-1);
    k_ztrans = Nztrans*Nr + j;
    SetBC_NewmannZ(A,Nz,Nr,k_ztrans,dz,+1);
  }

  // Apply homogeneous Newmann BCs in r for (zcis<z<ztrans,r=rpore)
  for (int i=Nzcis+1; i<Nztrans; i++) {
    k_rpore = i*Nr + Nrpore;
    SetBC_NewmannR(A,Nz,Nr,k_rpore,dr,-1);
  }

  // Apply homogeneous Dirichlet BCs for (zcis<z<ztrans,rpore<r<=rmax)
  for (int i=Nzcis+1; i<Nztrans; i++) {
    for (int j=Nrpore+1; j<Nr; j++) {
      k_inpore = i*Nr + j;
      SetBC_Dirichlet(A,Nz,Nr,k_inpore);
      f[k_inpore] = 1.;
    }
  }

  ////
  ////////////////////////////////////////////////////////////////

  //// DON'T CHANGE THIS PART
  //// Solve, output, exit
  // Solve using LAPACK
  SolveSystem(A,u,f,Nz,Nr);
  // Output
  char fout_name[256]; sprintf(fout_name,"data/soln_%03d_%03d.dat",Nr,(int) round(rmax));
  FILE* fout =fopen(fout_name,"w");
  for (int i=0; i<Nz*Nr; i++) {
    fprintf(fout,"%.8e\n",u[i]);
  }
  // Release Memory
  fclose(fout); free(f); free(u); 
  for (int i=0; i<Nz*Nr; i++) {
    free(A[i]);
  } free(A);
  // Exit
  return 0;
  ////
}
