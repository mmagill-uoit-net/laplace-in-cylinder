#include "laplace-in-cylinder.h"

void Print1DVector(double* u, int Nz, int Nr) {
  for (int i=0; i<Nz*Nr; i++) {
    printf("%.1f\n",u[i]); }
  printf("\n");
}

void Print2DArray(double** A, int Nz, int Nr) {
  for (int i=0; i<Nz*Nr; i++) {
    for (int j=0; j<Nz*Nr; j++) { 
      printf("%.1f\t",A[i][j]); 
    }
    printf("\n");
  } 
  printf("\n");
}

void Print1DArray(double* AT, int Nz, int Nr) {
  for (int i=0; i<Nz*Nr; i++) {
    for (int j=0; j<Nz*Nr; j++) { 
      printf("%.1f\t",AT[j+Nz*Nr*i]); 
    }
    printf("\n");
  }
  printf("\n");
}

////

void FillOperatorWoBcs(double** A,int Nz,int Nr,double dz,double dr) {

  // Fill the operator as if there were no BCs

  // del f = f_zz + (1/r) f_r + f_rr
  // f_zz ~ (f_z+1 - 2 f_z + f_z-1) / dz^2
  // f_rr ~ (f_r+1 - 2 f_r + f_r-1) / dr^2
  // f_r  ~ (f_r+1 - f_r) / dr
  double r;
  int k;
  for (int i=1; i<Nz-1; i++) {
    for (int j=1; j<Nr-1; j++) {
      k = i*Nr + j;
      r = j*dr;
      // Current position
      A[k][k] = (-2./(dz*dz)) + (-2./(dr*dr)) + (-1./dr)/r;
      // Neighbours in r
      A[k][k-1] = (1./(dr*dr));
      A[k][k+1] = (1./(dr*dr)) + (1./dr)/r;
      // Neighbours in z
      A[k][k-Nr] = (1./(dz*dz));
      A[k][k+Nr] = (1./(dz*dz));      
    }
  }

}

////

void SolveSystem(double** A,double* u,double* f,int Nz,int Nr) {

  // Misc Declarations
  int ok; int NRHS = 1;
  int N = Nz*Nr;
  int* pivot = malloc(sizeof(int)*N);

  // Transform from 2D operator to transposed 1D operator
  double* AT = malloc(sizeof(double)*N*N);
  for (int i=0; i<N; i++) {
    for(int j=0; j<N; j++) {
      AT[j+N*i]=A[j][i];		
    }
  }
  //  Print1DArray(AT,Nz,Nr);

  // Prefill solution vector with RHS vector
  for (int i=0; i<N; i++) {
    u[i] = f[i];
  }
  // Print1DVector(f,Nz,Nr);

  // Solve using this retarded notation...
  dgesv_(&N,&NRHS,AT,&N,pivot,u,&N,&ok);
  //  Print1DVector(u,Nz,Nr);

  // Release Memory
  free(pivot); free(AT);

}

////

void ClearRow(double** A,int Nz,int Nr,int row) {
  int col;
  for (int i=0; i<Nz; i++) {
    for (int j=0; j<Nr; j++) {
      col = i*Nr + j;
      A[row][col] = 0.0;
    }
  }
}

void SetBC_Dirichlet(double** A,int Nz,int Nr,int k) {
  ClearRow(A,Nz,Nr,k);
  A[k][k] = 1.;
}

void SetBC_NewmannR(double** A,int Nz,int Nr,int k,double dr,int direct) {
  ClearRow(A,Nz,Nr,k);
  A[k][k] = (-1./dr);
  A[k][k+direct] = (1./dr);
}

void SetBC_NewmannZ(double** A,int Nz,int Nr,int k,double dz,int direct) {
  ClearRow(A,Nz,Nr,k);
  A[k][k] = (-1./dz);
  A[k][k+direct*Nr] = (1./dz);
}
