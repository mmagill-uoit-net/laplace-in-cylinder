import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D



# Select case
Nr = 64
Nz = 2*Nr

# Load data
u = np.loadtxt('data/soln_%d.dat'%Nr)
U = np.resize(u,(Nz,Nr))

# Coordinates
r = np.linspace(0.,4.,Nr)
z = np.linspace(0.,8.,Nz)
R = np.outer(np.ones(Nz),r)
Z = np.outer(z,np.ones(Nr))

# DON'T THINK THIS WORKS
# Calculate gradient (cylindrical coordinates with symmetry means this is okay)
dr = r[1] - r[0]
dz = z[1] - z[0]
Ez,Er = np.gradient(-U,dz,dr)



# Double-side everything
UU = np.concatenate((np.fliplr(U),U),axis=1)
RR = np.concatenate((-np.fliplr(R),R),axis=1)
ZZ = np.concatenate((np.fliplr(Z),Z),axis=1)
EEr = np.concatenate((-np.fliplr(Er),Er),axis=1)
EEz = np.concatenate((np.fliplr(Ez),Ez),axis=1)

# Plot
fig = plt.figure()

#ax = plt.axes(projection='3d')
#ax.plot_surface(RR, ZZ, UU, cmap=plt.cm.jet, rstride=1, cstride=1, linewidth=0)

ax = plt.axes()
ax.pcolor(RR,ZZ,UU)
ax.contour(RR,ZZ,UU,20,colors='k',lw=6)

#ax = plt.axes()
ax.streamplot(RR,ZZ,EEr,EEz)

plt.show()


