
#ifndef _LAPLACEINCYLINDER_H_
#define _LAPLACEINCYLINDER_H_



#include <stdio.h>
#include <stdlib.h>
#include <math.h>

// Declare LAPACK solver
void dgesv_(const int *N, const int *nrhs, double *A, const int *lda, 
	    int *ipiv, double *b, const int *ldb, int *info);


// My functions
void Print1DVector(double* u,int Nz,int Nr);
void Print2DArray(double** A,int Nz,int Nr);
void Print1DArray(double* AT,int Nz,int Nr);
void FillOperatorWoBcs(double** A,int Nz,int Nr,double dz,double dr);
void SolveSystem(double** A,double* u,double* f,int Nz,int Nr);

void ClearRow(double** A,int Nz,int Nr,int row);
void SetBC_Dirichlet(double** A,int Nz,int Nr,int k);
void SetBC_NewmannR(double** A,int Nz,int Nr,int k,double dr,int direct);
void SetBC_NewmannZ(double** A,int Nz,int Nr,int k,double dz,int direct);



#endif
