#include "laplace-in-cylinder.h"


int main(int argc, char* argv[]) {

  ////////////////////////////////////////////////////////////////
  //// Define domain, mesh, CLI
  if ( argc != 2 ) { printf("%s [Nr]\n",argv[0]); return 1; }
  double rmax = 4.0;
  double zmax = 2.0*rmax;
  int Nr = strtol(argv[1],NULL,10); 
  int Nz = 2*Nr;
  ////
  ////////////////////////////////////////////////////////////////

  //// DON'T CHANGE THIS PART
  //// Set up A,f,u and initialize w/o BCs
  // Initialize operator A, RHS vector F, and solution U
  double** A = malloc(sizeof(double*)*Nz*Nr); 
  double* f = malloc(sizeof(double)*Nz*Nr); 
  double* u = malloc(sizeof(double)*Nz*Nr);
  for (int i=0; i<Nz*Nr; i++) {
    A[i] = malloc(sizeof(double)*Nz*Nr);
    for (int j=0; j<Nz*Nr; j++) { A[i][j] = 0; }
    f[i] = 0; u[i] = 0;
  }
  // Fill operator, ignoring boundaries
  double dz = zmax / (double) Nz; double dr = rmax / (double) Nr;
  FillOperatorWoBcs(A,Nz,Nr,dz,dr);
  ////

  ////////////////////////////////////////////////////////////////
  //// Define geometry, BCs
  //// Be careful about applying BCs in the same row twice
  //// e.g. Newmann then Dirichlet w.o reinitializing is wrong
  //// All conditions are homogeneous by default

  // Geometric parameters
  double rpore = rmax/4.0;
  int Nrpore = (int) floor(Nr * rpore/rmax);
  int k_z0; int k_zmax;
  int k_r0; int k_rmax;

  // Apply homogenous Newmann BCs in z for (z={0,zmax},rpore<=r<=rmax)
  for (int j=Nrpore; j<Nr; j++) {
    k_z0 = 0*Nr + j;
    SetBC_NewmannZ(A,Nz,Nr,k_z0,dz,+1);
    k_zmax = (Nz-1)*Nr + j;
    SetBC_NewmannZ(A,Nz,Nr,k_zmax,dz,-1);
  }

  // Apply homogenous Newmann BCs in r for (interior z,r={0,rmax})
  for (int i=1; i<Nz-1; i++) {
    k_r0 = i*Nr + 0;
    SetBC_NewmannR(A,Nz,Nr,k_r0,dr,+1);
    k_rmax = i*Nr + (Nr-1);
    SetBC_NewmannR(A,Nz,Nr,k_rmax,dr,-1);
  }

  // Apply non-homogeneous Dirichlet BCs for (z={0,zmax},r<rpore)
  for (int j=0; j<Nrpore; j++) {
    k_z0 = 0*Nr + j;
    SetBC_Dirichlet(A,Nz,Nr,k_z0);
    f[k_z0] = 1.;
    k_zmax = (Nz-1)*Nr + j;
    SetBC_Dirichlet(A,Nz,Nr,k_zmax);
    f[k_zmax] = -1.;
  }

  ////
  ////////////////////////////////////////////////////////////////

  //// DON'T CHANGE THIS PART
  //// Solve, output, exit
  // Solve using LAPACK
  SolveSystem(A,u,f,Nz,Nr);
  // Output
  char fout_name[256]; sprintf(fout_name,"data/soln_%d.dat",Nr);
  FILE* fout =fopen(fout_name,"w");
  for (int i=0; i<Nz*Nr; i++) {
    fprintf(fout,"%.8e\n",u[i]);
  }
  // Release Memory
  fclose(fout); free(f); free(u); 
  for (int i=0; i<Nz*Nr; i++) {
    free(A[i]);
  } free(A);
  // Exit
  return 0;
  ////
}
